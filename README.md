# Readme
## Environment Setup
### Prerequisites
On Mac OS, Xcode is needed and all licenses must be accepted. Additionally, [CocoaPods](https://cocoapods.org/) is required.
On all platforms, an installation of [Android](https://developer.android.com/studio/#downloads) version >= 21 is required, 
as well as a recent installation of [NodeJS](https://nodejs.org/en/download/) and [Yarn](https://yarnpkg.com/getting-started/install).

If not using a shared development certificate, you will need to change the bundle id
of the ios app.

Clone the repo:
```bash
git clone https://gitlab.com/immackay/agvisor-pro.git && cd agvisor-pro
```

Install dependencies:
```bash
yarn install
cd ios && pod install
```

### Recommendations

Webstorm features built-in support for typescript, react, and eslint. Highly recommended.

#### Visual Studio Code

Install plugin `dbaeumer.vscode-eslint` (ESLint by Dirk Baeumer). 

Additionally, you can enable it as a formatter by setting `"eslint.format.enable": true` in your user or workspace settings, then set a key binding or enable format on save to run `eslint --fix`.

## Running on Devices/Simulators
### iOS
First you will need to open the AGvisorPRO xcode workspace and select a development team under the `Signing & Capabilities` tab.

```bash
yarn ios
```

### Android
Substitute $ANDROID_SDK_ROOT with $ANDROID_HOME on older installations.

#### Emulator Setup
##### API 29
```bash
$ANDROID_SDK_ROOT/tools/bin/sdkmanager 'system-images;android-29;google_apis_playstore;x86' 'emulator'
$ANDROID_SDK_ROOT/tools/bin/avdmanager create avd -c 1G -n API_29 -d 19 -k 'system-images;android-29;google_apis_playstore;x86'
```

```bash
yarn android
```

##### API 21 (used for testing)
```bash
$ANDROID_SDK_ROOT/tools/bin/sdkmanager 'system-images;android-21;google_apis;x86'
$ANDROID_SDK_ROOT/tools/bin/avdmanager create avd -c 1G -n API_21 -d 19 -k 'system-images;android-21;google_apis;x86'
```

### Manual
#### iOS
```bash
NAME="DeviceName" yarn install:debug:ios
```
NAME eg "Ian's iPhone" or "iPhone 8"

Alternatively you can install via XCode.

#### Android
Install on emulator:
```bash
$ANDROID_SDK_ROOT/emulator/emulator -avd API_29
yarn install:debug:android
```

To install on a local device, connect your device and enable USB debugging after starting `adb server` or running `adb devices`.
Then `yarn install:debug:android` to install.

To connect to the devices console, run `adb logcat`.

Alternatively you can use IntelliJ/Android Studio.

### E2E Tests

See [Detox Getting Started guide](https://github.com/wix/Detox/blob/master/docs/Introduction.GettingStarted.md) 
and [Android specific information](https://github.com/wix/Detox/blob/master/docs/Introduction.Android.md).

Ensure you have run `yarn build:test:ios` before running `yarn test:e2e:ios` or `yarn test:all`  
Ensure you have run `yarn build:test:android` before running `yarn test:e2e:android` or `yarn test:all`

You'll need an android emulator named API_29 to run `yarn test:e2e:android` or `yarn test:all`

## Notes

- There's no backend of any sort, all data is faked (and constant)
- Most things don't do anything

## Screenshots

<img src="/docs/screenshots/home.PNG" alt="Home Screen" width="350" />
<br />
<img src="/docs/screenshots/agvisors.PNG" alt="AGvisors Screen" width="350" />
<br />
<img src="/docs/screenshots/agvisors_search.PNG" alt="AGvisors Search" width="350" />
<br />
<img src="/docs/screenshots/menu.PNG" alt="Menu" width="350" />
<br />
<img src="/docs/screenshots/menu_sub.PNG" alt="[Menu with sub menu" width="350" />
