module.exports = (api) => {
  api.cache(() => process.env.NODE_ENV);

  const productionConfig = {
    plugins: ['babel-plugin-transform-remove-console'],
  };

  return {
    presets: ['module:metro-react-native-babel-preset'],
    plugins: [
      [
        '@babel/plugin-proposal-decorators',
        {
          legacy: true,
        },
      ],
      'transform-node-filepath',
      [
        'babel-plugin-module-resolver',
        {
          root: ['./src/'],
          extensions: [
            '.js',
            '.ts',
            '.jsx',
            '.tsx',
            '.ios.js',
            '.ios.ts',
            '.ios.jsx',
            '.ios.tsx',
            '.android.js',
            '.android.ts',
            '.android.jsx',
            '.android.tsx',
          ],
        },
      ],
    ],
    env: {
      staging: productionConfig,
      production: productionConfig,
      test: productionConfig,
    },
  };
};
