import 'react-native';
import { render } from '@testing-library/react-native';
import { shallow } from 'enzyme';
import React from 'react';

import App from 'App';
import { testIDs as testIDsLoginScreen } from 'screens/auth/Login';

describe('Basic render methods', () => {
  it('should shallow render correctly', () => {
    const wrapper = shallow(<App />);
    expect(wrapper).toMatchSnapshot();
  });
  it('should full render correctly', () => {
    const json = render(<App />).asJSON();
    expect(json).toMatchSnapshot();
  });
  it('should display loading screen', () => {
    const { getByTestId } = render(<App />);

    expect(getByTestId('screens/Loading')).toBeTruthy();
  });
  it('should display login screen after a few seconds', () => {
    jest.useRealTimers();
    jest.setTimeout(10000);
    const { getByTestId } = render(<App />);

    setTimeout(
      () => expect(getByTestId(testIDsLoginScreen.base)).toBeTruthy(),
      5000,
    );
  });
});
