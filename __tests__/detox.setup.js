import detox from 'detox';

beforeAll(async () => {
  await detox.init(undefined, { initGlobals: false });
});
