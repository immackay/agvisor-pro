import { expect, device, element, by } from 'detox';

import { testIDs as testIDsLoginScreen } from 'screens/auth/Login';
import { testIDs as testIDsPersonalInformationScreen } from 'screens/auth/PersonalInformation';

describe('Example', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should have login screen', async () => {
    await expect(element(by.id(testIDsLoginScreen.base))).toBeVisible();
  });

  it('should show sign up screen after tap', async () => {
    await element(by.id(testIDsLoginScreen.createAccountButton)).tap();
    await expect(
      element(by.id(testIDsPersonalInformationScreen.base)),
    ).toBeVisible();
  });
});
