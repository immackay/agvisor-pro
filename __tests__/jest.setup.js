import 'react-native';
import 'jest-enzyme';

import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { JSDOM } from 'jsdom';

// Setup JSDOM
const jsdom = new JSDOM('<!doctype html><html lang="en"><body/></html>');
const { window } = jsdom;

const copyProps = (src, target) =>
  Object.defineProperties(target, {
    ...Object.getOwnPropertyDescriptors(src),
    ...Object.getOwnPropertyDescriptors(target),
  });

const assignToGlobal = (descriptor, object) => (global[descriptor] = object);

assignToGlobal('window', window);
assignToGlobal('document', window.document);

assignToGlobal('navigator', { userAgent: 'node.js' });
assignToGlobal('requestAnimationFrame', (c) => setImmediate(c));
assignToGlobal('cancelAnimationFrame', (id) => clearTimeout(id));
copyProps(window, global);

// Configure Enzyme

Enzyme.configure({ adapter: new Adapter() });
