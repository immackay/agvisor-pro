import mockAsyncStorage from '@react-native-community/async-storage/jest/async-storage-mock';
import mockRNCNetInfo from '@react-native-community/netinfo/jest/netinfo-mock.js';
import mockRNReanimated from 'react-native-reanimated/mock';

// Mock libraries
jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');
jest.mock('react-native/Libraries/Utilities/DevSettings', () => ({
  addMenuItem: jest.fn(),
  reload: jest.fn(),
}));
jest.mock('@react-native-community/netinfo', () => mockRNCNetInfo);
jest.mock('react-native-gesture-handler');
jest.mock('react-native-reanimated', () => mockRNReanimated);
jest.mock('@react-native-community/async-storage', () => mockAsyncStorage);
