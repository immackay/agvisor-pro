import _ from 'lodash';

// TODO convert to lazy
export const getModulePath = (fullPath: string) =>
  _.last(fullPath.split('/src/'))!.replace(/\.[jt]sx?$/, '');
