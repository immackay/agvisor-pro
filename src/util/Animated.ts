import Animated, { Easing } from 'react-native-reanimated';

const {
  Value,
  block,
  Clock,
  cond,
  clockRunning,
  startClock,
  stopClock,
  timing,
  call,
  set,
} = Animated;

export const runTimingWithCallback = ({
  from,
  to,
  duration,
  easing,
  callback,
}: {
  from: number;
  to: number;
  duration?: number;
  easing?: Animated.EasingFunction;
  callback?: () => void;
}) => {
  const clock = new Clock();
  const state = {
    finished: new Value(0),
    position: new Value(0),
    time: new Value(0),
    frameTime: new Value(0),
  };

  const config = {
    duration: duration ?? 1000,
    toValue: new Value(0),
    easing: easing ?? Easing.inOut(Easing.ease),
  };

  return block([
    cond(clockRunning(clock), 0, [
      set(state.finished, 0),
      set(state.time, 0),
      set(state.position, from),
      set(state.frameTime, 0),
      set(config.toValue, to),
      startClock(clock),
    ]),
    timing(clock, state, config),
    cond(state.finished, [
      stopClock(clock),
      ...(callback ? [call([], callback)] : []),
    ]),
    state.position,
  ]);
};
