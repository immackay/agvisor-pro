import AsyncStorage from '@react-native-community/async-storage';
import { applySnapshot, getSnapshot } from 'mobx-state-tree';

import { auth } from 'stores/Auth';
import { user, advisorStore, clientStore } from 'stores/User';
import { Logger } from 'util/Logger';
import { getModulePath } from 'util/getModulePath';

const logger = new Logger(getModulePath(__filename));

export class Storage {
  private static errorsCallback = (name: string, message: string) => (
    errors?: Error[],
  ) => {
    if (errors) {
      errors.map((e) =>
        logger.warn(message, {
          item: e,
          name,
        }),
      );
    }
  };

  public static save = () => {
    if (auth.isAuthorized) {
      logger.info('Saving application state...', { name: 'save' });

      auth
        .save()
        .then((result) =>
          logger.info(
            result
              ? 'Successfully saved credentials'
              : 'Failed saving credentials',
            { name: 'save' },
          ),
        );

      const userJson = JSON.stringify(getSnapshot(user));
      const advisorStoreJson = JSON.stringify(getSnapshot(advisorStore));
      const clientStoreJson = JSON.stringify(getSnapshot(clientStore));

      AsyncStorage.multiSet([
        ['userPersistenceKey', userJson],
        ['advisorStorePersistenceKey', advisorStoreJson],
        ['clientStorePersistenceKey', clientStoreJson],
      ])
        .then(() =>
          logger.info('Successfully saved application state', {
            name: 'save',
          }),
        )
        .catch(
          Storage.errorsCallback(
            'save',
            'Received error while saving application state',
          ),
        );
    }
  };

  public static load = () => {
    logger.info('Loading application state...', { name: 'load' });

    auth
      .load()
      .then((result) =>
        logger.info(
          result
            ? 'Successfully loaded credentials'
            : 'Failed loading credentials',
          { name: 'load' },
        ),
      );

    AsyncStorage.multiGet([
      'userPersistenceKey',
      'advisorStorePersistenceKey',
      'clientStorePersistenceKey',
    ])
      .then((results) => {
        results.map(([key, value]) => {
          if (value) {
            switch (key) {
              case 'userPersistenceKey':
                logger.info('Restoring userPersistenceKey', {
                  item: value,
                  name: 'load',
                });
                applySnapshot(user, JSON.parse(value));
                break;
              case 'advisorStorePersistenceKey':
                applySnapshot(advisorStore, JSON.parse(value));
                break;
              case 'clientStorePersistenceKey':
                applySnapshot(clientStore, JSON.parse(value));
                break;
              default:
                logger.warn(`Received unknown AsyncStorage key: ${key}`, {
                  item: value,
                  name: 'load',
                });
            }
          }
        });

        logger.info('Finished loading application state', { name: 'load' });

        return true;
      })
      .catch(
        Storage.errorsCallback(
          'load',
          'Received error while loading application state',
        ),
      );
  };

  public static clear = () => {
    logger.info('Clearing application state...', { name: 'clear' });

    auth.clear().then(() =>
      logger.info('Cleared authentication credentials', {
        name: 'clear',
      }),
    );

    AsyncStorage.clear()
      .then(() => logger.info('Cleared application state', { name: 'clear' }))
      .catch((e) =>
        logger.warn('Received error while clearing application state', {
          item: e,
          name: 'clear',
        }),
      );
  };
}
