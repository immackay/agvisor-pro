/**
 * @typedef {Object} TestIDType
 * @property {string} key - key of testID
 * @property {(string|string[])} name - name of object
 * @property {(string|string[]|undefined)} path - path to object
 */
type TestIDType = {
  key: string;
  name: string | string[];
  path?: string | string[];
};

// unnecessary but w/e
export const TestID = (
  key: string,
  name: string | string[],
  path?: string | string[],
) => ({
  key,
  name,
  path,
});

// => 'screens/something/Example.SomethingInsideButNotDefault-SomeObjectName-someName'
const stringFromTestID = (path: string, id: TestIDType): string =>
  `${path}${id.path ? '.' : ''}${
    id.path instanceof Array
      ? id.path.reduce((a, c) => `${a}.${c}`)
      : id.path ?? ''
  }-${
    id.name instanceof Array ? id.name.reduce((a, c) => `${a}-${c}`) : id.name
  }`;

/**
 * see {@link file://../screens/auth/Login.tsx screens/auth/Login} for correct usage
 *
 * @param {string} path - filepath (use <code>getModulePath(__filename)</code>)
 * @param {Array<TestIDType>} ids - list of testIDs
 * @class
 */
export const TestIDs = (
  path: string,
  ...ids: Array<TestIDType>
): { [key: string]: string } => {
  const proto: { [key: string]: string } = {
    base: path,
  };

  ids.forEach((id) => (proto[id.key] = stringFromTestID(path, id)));

  return proto;
};
