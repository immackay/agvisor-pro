import moment from 'moment';

export enum LogLevel {
  ALL,
  VERBOSE,
  DEBUG,
  INFO,
  LOG,
  WARN,
  ERROR,
  NONE,
}

type LogData = {
  path?: string | string[];
  name?: string | string[];
  item?: any;
};
type LogFunction = (message: string, data?: LogData) => void;

interface ILogger {
  debug: LogFunction;
  info: LogFunction;
  log: LogFunction;
  warn: LogFunction;
  error: LogFunction;
  level: (level: LogLevel, message: string, data?: LogData) => void;
}

// in the future we may want to write a small babel plugin to remove all Logger calls in production
// see babel-plugin-transform-remove-console or babel-plugin-transform-node-filepath
export class Logger implements ILogger {
  public static LEVEL = LogLevel.INFO;
  public static TIMESTAMP = true;
  private static consoleEnabled = !!process.env.ENABLE_CONSOLE || __DEV__;
  private static isolatedConsoleLogs: string[] | undefined = undefined;

  constructor(private readonly path: string) {}

  private static readonly isAllowed = (level: LogLevel): boolean =>
    Logger.LEVEL !== LogLevel.NONE && level >= Logger.LEVEL;

  private static readonly timestamp = (): string =>
    moment().utc().format('Y-MM-DDTHH:mm:ss.SSS') + 'Z';

  private static readonly log = (
    path: string,
    level: LogLevel,
    message: string,
    type: 'log' | 'warn' | 'error' | 'debug' | 'info',
  ) => {
    if (Logger.isAllowed(level)) {
      if (
        Logger.consoleEnabled &&
        (Logger.isolatedConsoleLogs?.includes(path) ?? true)
      ) {
        if (type === 'info') console.info(message);
        else if (type === 'warn') console.warn(message);
        else if (type === 'error') console.error(message);
        else if (type === 'debug') console.debug(message);
        else console.log(message);
      }
    }
  };

  public static disableConsole = () => (Logger.consoleEnabled = false);

  public static isolateConsoleLogs = (paths: string[]) =>
    (Logger.isolatedConsoleLogs = paths);

  debug: LogFunction = (message, data) =>
    Logger.log(
      this.path,
      LogLevel.DEBUG,
      this.getMessage(LogLevel.DEBUG, message, data),
      'debug',
    );

  info: LogFunction = (message, data) =>
    Logger.log(
      this.path,
      LogLevel.INFO,
      this.getMessage(LogLevel.INFO, message, data),
      'info',
    );

  log: LogFunction = (message, data) =>
    Logger.log(
      this.path,
      LogLevel.LOG,
      this.getMessage(LogLevel.LOG, message, data),
      'log',
    );

  warn: LogFunction = (message, data) =>
    Logger.log(
      this.path,
      LogLevel.WARN,
      this.getMessage(LogLevel.WARN, message, data),
      'warn',
    );

  error: LogFunction = (message, data) =>
    Logger.log(
      this.path,
      LogLevel.ERROR,
      this.getMessage(LogLevel.ERROR, message, data),
      'error',
    );

  level = (level: LogLevel, message: string, data?: LogData) =>
    Logger.log(
      this.path,
      level,
      this.getMessage(level, message, data),
      level === LogLevel.INFO
        ? 'info'
        : level === LogLevel.WARN
        ? 'warn'
        : level === LogLevel.ERROR
        ? 'error'
        : 'log',
    );

  // => [INFO] 1593750923760 [screens/something/Example.SomethingInsideButNotDefault-SomeObjectName-someName] message
  // => Data: { something: 'foo', somethingUndefined: 'undefined' }
  private getMessage = (
    level: LogLevel,
    message: string,
    data?: LogData,
  ): string =>
    `[${LogLevel[level]}]${
      Logger.TIMESTAMP ? ' ' + `[${Logger.timestamp()}]` : ''
    } [${this.path}${data?.path ? '.' : ''}${
      data?.path instanceof Array
        ? data.path.reduce((a, c) => `${a}.${c}`)
        : data?.path ?? ''
    }${data?.name ? '-' : ''}${
      data?.name instanceof Array
        ? data.name.reduce((a, c) => `${a}-${c}`)
        : data?.name ?? ''
    }] ${message}${
      data?.item
        ? '\nData: ' +
          // remove last parameter if log size is becoming cumbersome (or remove item logging altogether)
          JSON.stringify(data.item, (key, value) => value ?? 'undefined', 2)
        : ''
    }`;
}
