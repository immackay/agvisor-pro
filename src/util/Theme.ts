import { Platform, TextStyle } from 'react-native';

export const getFont = ({
  fontFamily,
  fontWeight,
  fontStyle,
  ...rest
}: TextStyle) => {
  if (Platform.OS === 'android') {
    if (fontFamily === 'Raleway') {
      let family = fontFamily + '-';
      switch (fontWeight) {
        case '100':
          family += 'Thin';
          break;
        case '200':
          family += 'ExtraLight';
          break;
        case '300':
          family += 'Light';
          break;
        case undefined:
        case 'normal':
        case '400':
          if (fontStyle !== 'italic') family += 'Regular';
          break;
        case '500':
          family += 'Medium';
          break;
        case '600':
          family += 'SemiBold';
          break;
        case 'bold':
        case '700':
          family += 'Bold';
          break;
        case '800':
          family += 'ExtraBold';
          break;
        case '900':
          family += 'Black';
          break;
      }

      if (fontStyle === 'italic') family += 'Italic';

      return { fontFamily: family, ...rest };
    }
  }

  return { fontFamily, fontWeight, fontStyle, ...rest };
};
