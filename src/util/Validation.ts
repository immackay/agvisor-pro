import validate from 'validate.js';

import { Provinces } from 'stores/Auth';

// TODO go over messages and constraints

validate.validators.email.message = 'must be valid';

validate.validators.type.messages.province = 'must be selected';
validate.validators.type.types.province = (value: string) =>
  Object.keys(Provinces).includes(value);

const email = {
  email: true,
};
const password = {
  presence: true,
  length: {
    minimum: 8,
    message: 'must be at least 8 characters',
  },
};

const confirmPassword = {
  presence: true,
  equality: 'password',
};

const emailAndPassword = {
  email,
  password,
};

const firstName = {
  length: { minimum: 1, message: 'must not be blank' },
  presence: true,
};

const lastName = {
  length: { minimum: 1, message: 'must not be blank' },
  presence: true,
};

const name = {
  firstName,
  lastName,
};

const province = {
  presence: true,
  type: 'province',
};

export const Constraints = Object.freeze({
  login: {
    ...emailAndPassword,
  },
  signUp: {
    ...name,
    ...emailAndPassword,
    confirmPassword,
    province,
    agreedToTerms: {
      type: 'boolean',
      exclusion: {
        within: [false],
        message: '^You must agree to the terms & conditions',
      },
    },
  },
  forgotPassword: {
    email,
  },
  verification: {
    code: {
      type: 'integer',
      presence: true,
    },
  },
});
