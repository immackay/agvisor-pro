import React from 'react';
import Svg, { Path } from 'react-native-svg';

export default ({ color = '#fff' }: { color?: string }) => {
  return (
    <Svg fill="none" height={17} viewBox="0 0 24 17" width={24}>
      <Path
        d="M.327 7.462L7.469.327a1.117 1.117 0 011.578 0 1.114 1.114 0 010 1.576L3.81 7.135h19.074a1.115 1.115 0 110 2.23H3.81l5.237 5.232a1.114 1.114 0 01-.79 1.903c-.285 0-.57-.109-.788-.326L.327 9.038a1.114 1.114 0 010-1.576z"
        fill={color}
      />
    </Svg>
  );
};
