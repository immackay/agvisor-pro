import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

export default ({ color = '#aaa' }: { color?: string }) => {
  return (
    <Svg fill="none" height={28} viewBox="0 0 22 28" width={22}>
      <Path
        d="M1.25 11.04c0 5.14 3.967 9.366 9.012 9.761l-.025 3.193-.015 1.861 1.301-1.33 5.963-6.1a9.737 9.737 0 003.36-7.384c0-5.405-4.39-9.791-9.798-9.791-5.407 0-9.798 4.386-9.798 9.79z"
        stroke={color}
        strokeWidth={1.5}
      />
    </Svg>
  );
};
