import React from 'react';
import Svg, { Path } from 'react-native-svg';

export default ({
  color = '#000',
  size = 24,
}: {
  color?: string;
  size?: number;
}) => {
  return (
    <Svg
      fill="none"
      height={size}
      stroke={color}
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}
      viewBox="0 0 24 24"
      width={size}>
      <Path d="M20 6L9 17l-5-5" />
    </Svg>
  );
};
