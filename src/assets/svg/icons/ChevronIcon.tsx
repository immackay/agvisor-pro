import React from 'react';
import Svg, { G, Path } from 'react-native-svg';

export default ({
  size,
  color,
  rotate,
}: {
  size: number;
  color: string;
  rotate?: number;
}) => (
  <Svg height={size} viewBox="0 0 1000 1000" width={size}>
    <G fill={color} originX={500} originY={500} rotation={rotate}>
      <Path d="M256.4 379.6h492.8-492.8zM614.3 500L211.4 97.1 298.5 10l490 490-490 490-87.1-87.1L614.3 500z" />
    </G>
  </Svg>
);
