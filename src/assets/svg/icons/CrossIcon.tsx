import React from 'react';
import Svg, { Path } from 'react-native-svg';

export default ({ color = '#fff' }: { color?: string }) => {
  return (
    <Svg fill="none" height={22} viewBox="0 0 22 22" width={22}>
      <Path
        d="M10.044 10.934L1.19 19.787a.63.63 0 00.89.89l8.92-8.92 8.919 8.92a.627.627 0 00.89 0 .63.63 0 000-.89l-8.853-8.853 8.858-8.86a.63.63 0 10-.89-.89L11 10.11 2.075 1.185a.63.63 0 00-.89.89l8.859 8.859z"
        fill={color}
        stroke={color}
        strokeWidth={0.5}
      />
    </Svg>
  );
};
