import React from 'react';
import Svg, { Path } from 'react-native-svg';

export default () => {
  return (
    <Svg fill="none" height={193} viewBox="0 0 159 193" width={159}>
      <Path
        d="M79.441 0a79.299 79.299 0 00-56.107 23.1A79.236 79.236 0 000 79.093a79.233 79.233 0 0023.11 56.085 79.296 79.296 0 0056.014 23.325l-.241 34.318 52.387-53.655c19.376-19.984 27.384-35.982 27.384-59.915a79.233 79.233 0 00-23.197-56.014A79.297 79.297 0 0079.441 0z"
        fill="#da1c5c"
        opacity={0.7}
      />
    </Svg>
  );
};
