// TODO ensure all svgs are typed correctly

export type SVGType = (props: { color?: string; size?: number }) => JSX.Element;
