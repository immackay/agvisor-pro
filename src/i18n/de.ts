import { Translation } from './Translation';

const de: Translation = {
  navigation: {
    drawer: {
      addFarm: 'Füge eine Farm',
      becomeAgvisor: 'Werde AGvisor',
      favourites: 'Favoriten',
      faq: 'FAQ',
      feedback: 'Feedback',
      myProfile: 'Mein Profil',
      schedules: 'Zeitpläne',
      tour: 'Reise',
      support: 'Hilfe',
      signOut: 'Abmelden',
    },
  },
  locale: {
    CA: 'Kanada',
    'CA.AB': 'Alberta',
    'CA.BC': 'Britisch-Kolumbien',
    'CA.MB': 'Manitoba',
    'CA.NB': 'Neubraunschweig',
    'CA.NL': 'Neufundland und Labrador',
    'CA.NS': 'Neuschottland',
    'CA.NT': 'Nordwest-Territorien',
    'CA.NU': 'Nunavut',
    'CA.ON': 'Ontario',
    'CA.PE': 'Prinz-Eduard-Insel',
    'CA.QC': 'Québec',
    'CA.SK': 'Saskatchewan',
    'CA.YT': 'Yukon',
    US: 'Vereinigte Staaten von Amerika',
    'US.AL': 'Alabama',
  },
};

export default de;
