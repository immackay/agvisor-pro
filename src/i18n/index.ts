import i18n from 'i18n-js';
import { findBestAvailableLanguage } from 'react-native-localize';

import de from './de';
import en from './en';

i18n.fallbacks = true;
i18n.translations = { en, de };

const fallback = { languageTag: 'en', isRTL: false };

const { languageTag } =
  findBestAvailableLanguage(Object.keys(i18n.translations)) ?? fallback;

i18n.locale = languageTag;

export const translate = (key: string, options?: Record<string, unknown>) =>
  i18n.t(key, options);

export * from './Translation';
