import { Translation } from './Translation';

const en: Translation = {
  navigation: {
    drawer: {
      addFarm: 'Add A Farm',
      becomeAgvisor: 'Become An AGvisor',
      favourites: 'Favourites',
      faq: 'FAQ',
      feedback: 'Feedback',
      myProfile: 'My Profile',
      schedules: 'Schedules',
      tour: 'Tour',
      support: 'Support',
      signOut: 'Sign Out',
    },
  },
  locale: {
    CA: 'Canada',
    'CA.AB': 'Alberta',
    'CA.BC': 'British Columbia',
    'CA.MB': 'Manitoba',
    'CA.NB': 'New Brunswick',
    'CA.NL': 'Newfoundland and Labrador',
    'CA.NS': 'Nova Scotia',
    'CA.NT': 'Northwest Territories',
    'CA.NU': 'Nunavut',
    'CA.ON': 'Ontario',
    'CA.PE': 'Prince Edward Island',
    'CA.QC': 'Québec',
    'CA.SK': 'Saskatchewan',
    'CA.YT': 'Yukon',
    US: 'United States of America',
    'US.AL': 'Alabama',
  },
};

export default en;
