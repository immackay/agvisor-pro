// TODO go over implementation
// TODO allow fallback to english (currently limited only by Translation type I believe - ian)

// this implementation will likely become cumbersome
const locales = {
  CA: 'locale.CA',
  'CA.AB': 'locale.CA.AB',
  'CA.BC': 'locale.CA.BC',
  'CA.MB': 'locale.CA.MB',
  'CA.NB': 'locale.CA.NB',
  'CA.NL': 'locale.CA.NL',
  'CA.NS': 'locale.CA.NS',
  'CA.NT': 'locale.CA.NT',
  'CA.NU': 'locale.CA.NU',
  'CA.ON': 'locale.CA.ON',
  'CA.PE': 'locale.CA.PE',
  'CA.QC': 'locale.CA.QC',
  'CA.SK': 'locale.CA.SK',
  'CA.YT': 'locale.CA.YT',
  US: 'locale.US',
  'US.AL': 'locale.US.AL',
};

export type Translation = typeof Translations;
// use with translate => translate(AppStrings.drawer.addFarm)
export const Translations = Object.freeze({
  navigation: {
    drawer: {
      addFarm: 'navigation.drawer.addFarm',
      becomeAgvisor: 'navigation.drawer.becomeAgvisor',
      favourites: 'navigation.drawer.favourites',
      faq: 'navigation.drawer.faq',
      feedback: 'navigation.drawer.feedback',
      myProfile: 'navigation.drawer.myProfile',
      schedules: 'navigation.drawer.schedules',
      tour: 'navigation.drawer.tour',
      support: 'navigation.drawer.support',
      signOut: 'navigation.drawer.signOut',
    },
  },
  locale: locales,
});
