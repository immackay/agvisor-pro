import { NavigationContainerRef } from '@react-navigation/native';
import React, { useEffect, useCallback, useRef } from 'react';
import { StatusBar, AppState, DevSettings } from 'react-native';
import RNBootSplash from 'react-native-bootsplash';

import { fakeAdvisor } from 'fake/FakeAdvisor';
import { fakeClient } from 'fake/FakeClient';
import { Navigation } from 'screens/navigation';
import { AuthProvider, auth } from 'stores/Auth';
import {
  Advisor,
  AdvisorStoreProvider,
  Client,
  ClientStoreProvider,
  UserProvider,
  advisorStore,
  clientStore,
} from 'stores/User';
import { Logger } from 'util/Logger';
import { Storage } from 'util/Storage';
import { getModulePath } from 'util/getModulePath';

const logger = new Logger(getModulePath(__filename));

// TODO remove demo content
const DEMO_ADVISORS = 50;
for (let i = 0; i < DEMO_ADVISORS; i++) {
  clientStore.addClient(Client.create(fakeClient()));
  advisorStore.addAdvisor(Advisor.create(fakeAdvisor()));
}

export default (): JSX.Element => {
  const navigationRef = useRef<NavigationContainerRef>(null);

  const handleAppStateChange = useCallback((newState?: string) => {
    logger.info(`Received AppState change: ${newState}`, {
      name: 'handleAppStateChange',
    });
    if (newState === 'active') Storage.load();
    else if (newState === 'inactive' || newState === 'background')
      Storage.save();
  }, []);

  useEffect(() => {
    RNBootSplash.hide({ duration: 250 });

    AppState.addEventListener('change', handleAppStateChange);

    Storage.load();

    if (__DEV__) {
      DevSettings.addMenuItem('Clear persistence & Logout', () => {
        auth.signOut().then(Storage.clear);
        navigationRef.current?.resetRoot();
      });
    }

    return () => {
      AppState.removeEventListener('change', handleAppStateChange);

      Storage.save();
    };
  }, [handleAppStateChange]);

  // TODO all providers could be condensed into one component for easier management
  return (
    <AuthProvider>
      <UserProvider>
        <ClientStoreProvider>
          <AdvisorStoreProvider>
            <StatusBar barStyle="light-content" />
            <Navigation ref={navigationRef} />
          </AdvisorStoreProvider>
        </ClientStoreProvider>
      </UserProvider>
    </AuthProvider>
  );
};
