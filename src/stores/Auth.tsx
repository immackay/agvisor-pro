import AsyncStorage from '@react-native-community/async-storage';
import { action, computed, flow, observable } from 'mobx';
import React, { createContext, PropsWithChildren, useContext } from 'react';
import Auth0, { PasswordRealmResponse } from 'react-native-auth0';

import { Logger } from 'util/Logger';
import { getModulePath } from 'util/getModulePath';

// TODO cleanup
// TODO account for all auth0 errors

const logger = new Logger(getModulePath(__filename));

const auth0Credentials = {
  clientId: 'gS8tPG7KVOEYeX7asHsJYqXtnCpPx3qj',
  domain: 'ian-agvisorpro.us.auth0.com',
};

const auth0 = new Auth0(auth0Credentials);

export type Province = keyof typeof Provinces;
export const Provinces = Object.freeze({
  AB: 'Alberta',
  BC: 'British Columbia',
  MB: 'Manitoba',
  NB: 'New Brunswick',
  NL: 'Newfoundland and Labrador',
  NS: 'Nova Scotia',
  NT: 'Northwest Territories',
  NU: 'Nunavut',
  ON: 'Ontario',
  PE: 'Prince Edward Island',
  QC: 'Québec',
  SK: 'Saskatchewan',
  YT: 'Yukon',
});

export type AuthStates =
  | 'signUp'
  | 'signedIn'
  | 'signIn'
  | 'signedOut'
  | 'signedUp'
  | 'confirmSignIn'
  | 'confirmSignUp'
  | 'loading'
  | 'forgotPassword'
  | 'verifyContact';

export type AuthResultTypes =
  | 'login'
  | 'signOut'
  | 'signUp'
  | 'verification'
  | 'forgotPassword';

export interface AuthError extends Error {
  code?: string;
}

export type AuthResult = {
  success: boolean;
  type: AuthResultTypes;
  message?: string;
  error?: AuthError;
  errorCode?: string;
  redirect?: AuthStates;
};

export type UserParams = {
  firstName: string;
  lastName: string;
  profile?: string;
  profileData?: string;
  province: Province;
};

class Authentication {
  @observable
  private _email?: string;

  @observable
  private _id?: string;

  @observable
  private _credentials?: PasswordRealmResponse;

  @computed get id() {
    return this._id;
  }

  @computed get credentials() {
    return this._credentials;
  }

  @computed get isAuthorized() {
    return !!this._credentials;
  }

  public loginWithEmailAndPassword = flow(function* (
    this: Authentication,
    email: string,
    password: string,
  ): Generator<Promise<any>, AuthResult, any> {
    logger.info(`Attempting login on user ${email}`, {
      name: 'loginWithEmailAndPassword',
    });

    let result: AuthResult;

    try {
      const credentials = yield auth0.auth.passwordRealm({
        username: email,
        password,
        realm: 'Username-Password-Authentication',
        scope: 'openid profile email',
        audience: `https://${auth0Credentials.domain}/userinfo`,
      });

      logger.log('Credentials', {
        item: credentials,
        name: 'loginWithEmailAndPassword',
      });

      this._email = email;
      this._credentials = credentials;

      const { emailVerified, sub } = yield auth0.auth.userInfo({
        token: credentials.accessToken,
      });

      if (emailVerified) {
        logger.info(
          `Successfully logged in user ${email}: ${credentials.idToken}`,
          {
            name: 'loginWithEmailAndPassword',
          },
        );

        this._id = sub;

        result = {
          success: true,
          type: 'login',
        };
      } else {
        logger.info(`User ${email} not verified!`, {
          name: 'loginWithEmailAndPassword',
        });

        result = {
          success: false,
          type: 'login',
          message: 'User not confirmed',
          redirect: 'verifyContact',
        };
      }
    } catch (e) {
      logger.warn(`Received error while logging in user ${email}`, {
        name: 'loginWithEmailAndPassword',
        item: e,
      });

      // need to determine auth0 errors
      switch (e.code) {
        default:
          result = {
            success: false,
            type: 'login',
            message: 'An error occurred',
            error: e,
          };
      }
    }

    return result;
  });

  public signUpWithEmailAndPassword = flow(function* (
    email: string,
    password: string,
    data: UserParams,
  ): Generator<Promise<any>, AuthResult, any> {
    logger.info(`Attempting signup with email ${email}`, {
      name: 'signUpWithEmailAndPassword',
    });

    try {
      yield auth0.auth.createUser<UserParams>({
        email,
        password,
        connection: 'Username-Password-Authentication',
        metadata: data,
      });

      logger.info(`Successfully created user ${email}`, {
        item: data,
        name: 'signUpWithEmailAndPassword',
      });

      return {
        success: true,
        type: 'signUp' as AuthResultTypes,
      };
    } catch (e) {
      logger.warn(`Received error while signing up user ${email}`, {
        item: e,
        name: 'signUpWithEmailAndPassword',
      });

      switch (e.code) {
        case 'invalid_password':
          return {
            success: false,
            type: 'signUp' as AuthResultTypes,
            message: 'Password is too weak',
            errorCode: e.code,
          };
        default:
          return {
            success: false,
            type: 'signUp' as AuthResultTypes,
            message: 'An error occurred',
            error: e,
          };
      }
    }
  });

  public sendVerificationEmail = async (): Promise<AuthResult> => {
    // TODO implement this

    return {
      success: false,
      type: 'verification',
    };
  };

  public save = async (): Promise<boolean> => {
    if (this._credentials && this._email) {
      try {
        await AsyncStorage.setItem(
          'credentialPersistenceKey',
          JSON.stringify({
            ...this.credentials,
            email: this._email,
          }),
        );
        return true;
      } catch (e) {
        logger.warn('Received error while saving user credentials', {
          name: 'save',
          item: e,
        });
      }
    }
    return false;
  };

  @action
  public load = async (): Promise<boolean> => {
    try {
      const persistence = await AsyncStorage.getItem(
        'credentialPersistenceKey',
      );

      if (persistence) {
        const { email, ...credentials } = JSON.parse(persistence);

        this._email = email;
        this._credentials = credentials;

        return true;
      }
    } catch (e) {
      logger.warn('Received error while loading user credentials', {
        name: 'load',
        item: e,
      });
    }
    return false;
  };

  @action
  public clear = async (): Promise<void> => {
    try {
      await AsyncStorage.removeItem('credentialPersistenceKey');

      this._email = undefined;
      this._credentials = undefined;
    } catch (e) {
      logger.warn('Received error while clearing user credentials', {
        name: 'clear',
        item: e,
      });
    }
  };

  public signOut = async (): Promise<AuthResult> => {
    logger.info('Signing out current user', {
      name: 'signOut',
    });

    try {
      await this.clear();

      logger.info('Signed out current user', {
        name: 'signOut',
      });

      return {
        success: true,
        type: 'signOut',
      };
    } catch (e) {
      logger.warn('Received error while signing out current user', {
        name: 'signOut',
        item: e,
      });

      switch (e.code) {
        default:
          return {
            success: false,
            type: 'verification',
            message: 'An error occurred',
            error: e,
          };
      }
    }
  };
}

export const auth = new Authentication();

const AuthStoreContext = createContext<Authentication>(auth);

export const AuthProvider = ({ children }: PropsWithChildren<{}>) => (
  <AuthStoreContext.Provider value={auth}>{children}</AuthStoreContext.Provider>
);
export const AuthConsumer = AuthStoreContext.Consumer;
export const useAuth = () => useContext(AuthStoreContext);
