import { observable } from 'mobx';
import { flow, Instance, types } from 'mobx-state-tree';
import React, { PropsWithChildren, useContext } from 'react';

import { auth, Province, Provinces } from 'stores/Auth';
import { Logger } from 'util/Logger';
import { getModulePath } from 'util/getModulePath';

const logger = new Logger(getModulePath(__filename));

// TODO review implementation of this file, clean up

export enum OnlineModes {
  'OFFLINE' = 'OFFLINE',
  'BUSY' = 'BUSY',
  'AWAY' = 'AWAY',
  'ONLINE' = 'ONLINE',
}

export type UserPrototype = {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  profile: string | null;
  province: Province;
  online: OnlineModes;
};
export type UserInstance = Instance<typeof User>;

export const User = types
  .model('User', {
    identifier: types.optional(types.identifier, 'User'),
    authenticated: types.optional(types.boolean, false),
    data: types.optional(
      types.maybe(
        types.model({
          id: types.string,
          firstName: types.string,
          lastName: types.string,
          email: types.string,
          profile: types.maybeNull(types.string),
          province: types.enumeration<Province>(
            'Province',
            Object.keys(Provinces) as Province[],
          ),
          online: types.enumeration<OnlineModes>(
            'OnlineMode',
            Object.values(OnlineModes),
          ),
        }),
      ),
      undefined,
    ),
    extra: types.optional(
      types.model({
        rate: types.optional(types.maybeNull(types.string), null),
        sponsor: types.optional(types.maybeNull(types.string), null),
        description: types.optional(types.maybeNull(types.string), null),
        experience: types.optional(types.maybeNull(types.string), null),
        education: types.optional(types.maybeNull(types.string), null),
        knowledge: types.optional(
          types.maybeNull(types.array(types.string)),
          null,
        ),
      }),
      {},
    ),
  })
  .actions((self) => {
    const updateData = (data: UserPrototype) => (self.data = data);
    const getData = async (): Promise<UserPrototype | undefined> => {
      logger.info('Fetching data for current user', {
        path: 'User',
        name: 'getData',
      });

      try {
        return {
          id: 'example',
          email: 'test@example.com',
          firstName: 'Not',
          lastName: 'Implemented',
          profile: null,
          province: 'AB',
          online: OnlineModes.ONLINE,
        };
      } catch (e) {
        logger.warn('Received error while fetching data for current user', {
          item: e,
          path: 'User',
          name: 'getData',
        });
      }

      return undefined;
    };

    const login = flow(function* () {
      const id = auth.id;
      logger.info(`Logging in user ${id}`, { path: 'User', name: 'login' });

      try {
        updateData(yield getData());
        self.authenticated = true;

        logger.info(`Successfully logged in user ${id}`, {
          path: 'User',
          name: 'login',
        });
      } catch (e) {
        logger.warn(`Received error while logging in user ${id}`, {
          item: e,
          path: 'User',
          name: 'login',
        });
      }
    });

    const refresh = flow(function* () {
      logger.info('Refreshing current user', { path: 'User', name: 'refresh' });

      if (self.authenticated && self.data) {
        try {
          updateData(yield getData());
        } catch (e) {
          logger.warn(`Received error while logging in user ${self.data!.id}`, {
            item: e,
            path: 'User',
            name: 'refresh',
          });
        }

        logger.info('Successfully refreshed current user', {
          path: 'User',
          name: 'refresh',
        });
      } else {
        logger.warn('Tried to refresh current user while unauthenticated!', {
          path: 'User',
          name: 'refresh',
        });
      }
    });

    const setRate = (rate: string) => (self.extra.rate = rate);
    const setSponsor = (sponsor: string) => (self.extra.sponsor = sponsor);
    const setDescription = (description: string) =>
      (self.extra.description = description);
    const setExperience = (experience: string) =>
      (self.extra.experience = experience);
    const setEducation = (education: string) =>
      (self.extra.education = education);
    const addKnowledge = (knowledge: string) =>
      self.extra.knowledge
        ? self.extra.knowledge?.push(knowledge)
        : (self.extra.knowledge = observable([knowledge]));
    const removeKnowledge = (knowledge: string) =>
      self.extra.knowledge?.remove(knowledge);

    return {
      login,
      refresh,
      setRate,
      setSponsor,
      setDescription,
      setExperience,
      setEducation,
      addKnowledge,
      removeKnowledge,
    };
  })
  .views((self) => ({
    get id(): string | undefined {
      return self.data?.id;
    },
    get firstName(): string | undefined {
      return self.data?.firstName;
    },
    get lastName(): string | undefined {
      return self.data?.lastName;
    },
    get email(): string | undefined {
      return self.data?.email;
    },
    get profile(): string | null {
      return self.data?.profile ?? null;
    },
    get province(): keyof typeof Provinces | undefined {
      return self.data?.province;
    },
    get online(): OnlineModes | undefined {
      return self.data?.online;
    },
    get name() {
      return self.data
        ? self.data.firstName + ' ' + self.data.lastName
        : undefined;
    },
  }));

export const user = User.create({});

export const UserContext = React.createContext<UserInstance>(user);
export const UserProvider = ({ children }: PropsWithChildren<{}>) => (
  <UserContext.Provider value={user}>{children}</UserContext.Provider>
);
export const UserConsumer = UserContext.Consumer;
export const useUser = () => useContext(UserContext);

export type ClientInstance = Instance<typeof Client>;
export const Client = types
  .model('Client', {
    id: types.identifier,
    firstName: types.string,
    lastName: types.string,
    email: types.string,
    profile: types.maybeNull(types.string),
    province: types.enumeration<Province>(
      'Province',
      Object.keys(Provinces) as Province[],
    ),
    online: types.enumeration<OnlineModes>(
      'OnlineMode',
      Object.values(OnlineModes),
    ),
  })
  .views((self) => ({
    get name() {
      return self.firstName + ' ' + self.lastName;
    },
  }));

export type ClientStoreInstance = Instance<typeof ClientStore>;
export const ClientStore = types
  .model('ClientStore', {
    identifier: types.optional(types.identifier, 'ClientStore'),
    clients: types.optional(types.array(Client), []),
  })
  .views((self) => ({
    getClients: () => self.clients.toJS(),
    getClient: (id: string) => self.clients.find((c) => c.id === id),
  }))
  .actions((self) => ({
    addClient: (client: ClientInstance) => self.clients.push(client),
  }));

export const clientStore = ClientStore.create({});

export const ClientStoreContext = React.createContext<ClientStoreInstance>(
  clientStore,
);
export const ClientStoreProvider = ({ children }: PropsWithChildren<{}>) => (
  <ClientStoreContext.Provider value={clientStore}>
    {children}
  </ClientStoreContext.Provider>
);
export const ClientStoreConsumer = ClientStoreContext.Consumer;
export const useClientStore = () => useContext(ClientStoreContext);

export type AdvisorInstance = Instance<typeof Advisor>;
export const Advisor = types
  .model('Advisor', {
    id: types.identifier,
    firstName: types.string,
    lastName: types.string,
    email: types.string,
    profile: types.maybeNull(types.string),
    province: types.enumeration<Province>(
      'Province',
      Object.keys(Provinces) as Province[],
    ),
    online: types.enumeration<OnlineModes>(
      'OnlineMode',
      Object.values(OnlineModes),
    ),
    rate: types.maybeNull(types.string),
    sponsor: types.maybeNull(types.string),
    description: types.maybeNull(types.string),
    experience: types.maybeNull(types.string),
    education: types.maybeNull(types.string),
    knowledge: types.maybeNull(types.array(types.string)),
  })
  .views((self) => ({
    get name() {
      return self.firstName + ' ' + self.lastName;
    },
  }));

export type AdvisorStoreInstance = Instance<typeof AdvisorStore>;
export const AdvisorStore = types
  .model('AdvisorStore', {
    identifier: types.optional(types.identifier, 'TrainerStore'),
    advisors: types.optional(types.array(Advisor), []),
  })
  .views((self) => ({
    getAdvisors: () => self.advisors.toJS(),
    getAdvisor: (id: string) => self.advisors.find((a) => a.id === id),
  }))
  .actions((self) => ({
    addAdvisor: (advisor: AdvisorInstance) => self.advisors.push(advisor),
  }));

export const advisorStore = AdvisorStore.create({});

export const AdvisorStoreContext = React.createContext<AdvisorStoreInstance>(
  advisorStore,
);
export const AdvisorStoreProvider = ({ children }: PropsWithChildren<{}>) => (
  <AdvisorStoreContext.Provider value={advisorStore}>
    {children}
  </AdvisorStoreContext.Provider>
);
export const AdvisorStoreConsumer = AdvisorStoreContext.Consumer;
export const useAdvisorStore = () => useContext(AdvisorStoreContext);
