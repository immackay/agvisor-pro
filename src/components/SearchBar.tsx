import { IObservableArray } from 'mobx';
import { observer } from 'mobx-react';
import React, { useCallback, useContext } from 'react';
import { StyleSheet, View } from 'react-native';

import { SearchIcon } from 'assets/svg/icons';
import { Input } from 'components/Input';

export const SearchResultContext = React.createContext<
  SearchResultsType<unknown> | undefined
>(undefined);
export const SearchResultProvider = SearchResultContext.Provider;
export const SearchResultConsumer = SearchResultContext.Consumer;

export type SearchResultsType<S> = {
  data: IObservableArray<{ item: S; visible: boolean }>;
};
export const SearchBar = observer(
  <S extends unknown>({
    placeholder,
    searchPredicate,
  }: {
    placeholder: string;
    searchPredicate: (value: S, query: string) => boolean;
  }) => {
    const results = useContext(SearchResultContext)! as SearchResultsType<S>;

    const update = useCallback(
      (q: string) => {
        if (q !== '')
          results.data.forEach((i) => (i.visible = searchPredicate(i.item, q)));
        else results.data.forEach((i) => (i.visible = true));
      },
      [searchPredicate, results],
    );
    const onSubmitEditing = useCallback(
      ({ nativeEvent: { text } }) => update(text),
      [update],
    );

    return (
      <View style={styles.searchBar}>
        <Input
          height={45}
          IconLeft={SearchIcon}
          inputTextStyle={styles.inputText}
          placeholder={placeholder}
          onChangeText={update}
          onSubmitEditing={onSubmitEditing}
        />
      </View>
    );
  },
);

const styles = StyleSheet.create({
  inputText: {
    fontFamily: 'Raleway',
    fontSize: 14,
  },
  searchBar: {},
});
