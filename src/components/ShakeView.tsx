import React, {
  forwardRef,
  PropsWithChildren,
  Ref,
  useImperativeHandle,
  useMemo,
} from 'react';
import {
  InteractionManager,
  RegisteredStyle,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native';
import Animated, { Easing, Extrapolate } from 'react-native-reanimated';
import { useValue } from 'react-native-redash';

import { runTimingWithCallback } from 'util/Animated';

const { cond, eq, interpolate, Value } = Animated;

enum ShakeState {
  OFF,
  ON,
}

export type ShakeViewRef = {
  shake: () => void;
};

type ShakeViewStyle =
  | false
  | Animated.AnimateStyle<ViewStyle>
  | RegisteredStyle<Animated.AnimateStyle<ViewStyle>>
  | null
  | undefined;

export const ShakeView = forwardRef<
  ShakeViewRef,
  PropsWithChildren<{
    style?: ShakeViewStyle | readonly ShakeViewStyle[];
  }>
>(({ children, style }, ref: Ref<ShakeViewRef>) => {
  const shakeActive = useValue<ShakeState>(ShakeState.OFF);

  const shakeClock = useMemo(
    () =>
      cond(
        eq(shakeActive, ShakeState.OFF),
        0,
        runTimingWithCallback({
          from: 0,
          to: 1,
          duration: 500,
          easing: Easing.linear,
          callback: () => shakeActive.setValue(ShakeState.OFF),
        }),
      ),
    [shakeActive],
  );

  useImperativeHandle(ref, () => ({
    shake: () =>
      InteractionManager.runAfterInteractions(() =>
        shakeActive.setValue(
          cond(
            eq(shakeActive, ShakeState.OFF),
            new Value(ShakeState.ON),
            shakeActive,
          ),
        ),
      ),
  }));

  return (
    <View style={styles.container}>
      <Animated.View
        style={StyleSheet.flatten([
          style,
          {
            transform: [
              {
                translateX: interpolate(shakeClock, {
                  inputRange: [0, 0.2, 0.4, 0.6, 0.8, 1.0],
                  outputRange: [0, 15, -15, 7, -7, 0],
                  extrapolate: Extrapolate.CLAMP,
                }),
              },
            ],
          },
        ])}>
        {children}
      </Animated.View>
    </View>
  );
});

const styles = StyleSheet.create({
  container: { marginHorizontal: -15, paddingHorizontal: 15 },
});
