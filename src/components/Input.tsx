import React, {
  forwardRef,
  Fragment,
  PropsWithRef,
  Ref,
  RefObject,
  useCallback,
  useImperativeHandle,
  useMemo,
  useRef,
  useState,
  memo,
} from 'react';
import {
  InteractionManager,
  NativeSyntheticEvent,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TextInputProps,
  TextInputSubmitEditingEventData,
  TextStyle,
  View,
  ViewStyle,
} from 'react-native';

import { SVGType } from 'assets/svg/Types';
import { ErrorMessage, ErrorMessageRef } from 'components/ErrorMessage';
import { ShakeView, ShakeViewRef } from 'components/ShakeView';
// import { Logger } from 'util/Logger';
import { getFont } from 'util/Theme';

// const logger = new Logger('components/Input');

export type InputRef = {
  readonly value: string;
  clear(): void;
  shake(message?: string): void;
};
export type InputProps = PropsWithRef<
  {
    label?: string;
    placeholder?: string;
    height?: number;
    IconLeft?: SVGType;
    onChangeText?: (text: string) => void;
    onSubmitEditing?: (
      e: NativeSyntheticEvent<TextInputSubmitEditingEventData>,
    ) => void;
    inputTextStyle?: TextStyle;
    labelTextStyle?: TextStyle;
    containerStyle?: ViewStyle & {
      marginBottom?: number;
      marginVertical?: number;
    };
  } & Omit<
    TextInputProps,
    | 'placeholder'
    | 'placeholderTextColor'
    | 'style'
    | 'value'
    | 'onChangeText'
    | 'onSubmitEditing'
  >
>;

export const Input = memo(
  forwardRef(
    (
      {
        label,
        placeholder,
        height = 50,
        IconLeft,
        onChangeText,
        onSubmitEditing,
        inputTextStyle,
        labelTextStyle,
        containerStyle,
        ...rest
      }: InputProps,
      ref: Ref<InputRef>,
    ) => {
      let inputRef: RefObject<TextInput> = useRef(null);
      const [value, setValue] = useState('');
      const shakeRef = useRef<ShakeViewRef>(null);
      const errorRef = useRef<ErrorMessageRef>(null);

      // callbacks
      const renderIconLeft = useCallback(
        () => (IconLeft ? <IconLeft color={'#000'} size={20} /> : <Fragment />),
        [IconLeft],
      );
      const renderLabel = useCallback(
        () =>
          label ? (
            <Text
              style={[styles.label, labelTextStyle && getFont(labelTextStyle)]}>
              {label}
            </Text>
          ) : (
            <Fragment />
          ),
        [label, labelTextStyle],
      );
      const changeText = useCallback(
        (text) => {
          onChangeText?.(text);
          setValue(text);
        },
        [onChangeText],
      );

      // instance methods
      useImperativeHandle(
        ref,
        () => ({
          get value() {
            return value;
          },
          clear: () => {
            setValue('');
            inputRef.current?.clear();
          },
          isFocused: () => inputRef.current?.isFocused(),
          blur: () => inputRef.current?.blur(),
          focus: () => inputRef.current?.focus(),
          shake: (message) =>
            InteractionManager.runAfterInteractions(() => {
              if (message) errorRef.current?.raise(message);

              shakeRef.current?.shake();
            }),
        }),
        [value],
      );

      // calculate stylesheets
      const flattened = useMemo(() => {
        const container = StyleSheet.flatten([
          styles.container,
          containerStyle,
          {
            marginBottom:
              (containerStyle?.marginBottom ??
                containerStyle?.marginVertical ??
                0) - 20,
          },
        ]);
        const input = StyleSheet.flatten([
          styles.input,
          inputTextStyle && getFont(inputTextStyle),
          {
            paddingLeft: IconLeft ? 10 : 2,
          },
        ]);

        return { container, input };
      }, [IconLeft, containerStyle, inputTextStyle]);

      return (
        <View style={flattened.container}>
          {renderLabel()}
          <ShakeView
            ref={shakeRef}
            style={[styles.inputContainer, { minHeight: height }]}>
            {renderIconLeft()}
            <TextInput
              ref={inputRef}
              placeholder={placeholder}
              placeholderTextColor={'#7C7C83'}
              style={flattened.input}
              value={value}
              onChangeText={changeText}
              onSubmitEditing={onSubmitEditing}
              {...rest}
            />
          </ShakeView>
          <ErrorMessage ref={errorRef} />
        </View>
      );
    },
  ),
);

export const EmailInput = forwardRef(
  (props: InputProps, ref: Ref<InputRef>) => (
    <Input
      ref={ref}
      autoCapitalize={'none'}
      autoCompleteType={'email'}
      clearButtonMode={'while-editing'}
      keyboardType={'email-address'}
      label={'Email Address'}
      placeholder={'Email Address'}
      textContentType={'emailAddress'}
      {...props}
    />
  ),
);

// TODO add 'see password' button and fix android implementation
export const PasswordInput = forwardRef(
  (props: InputProps, ref: Ref<InputRef>) => (
    <Input
      ref={ref}
      clearTextOnFocus
      secureTextEntry
      autoCapitalize={'none'}
      autoCompleteType={'password'}
      keyboardType={Platform.OS === 'ios' ? 'default' : 'visible-password'}
      label={'Password'}
      placeholder={'Password'}
      textContentType={'password'}
      {...props}
    />
  ),
);

const styles = StyleSheet.create({
  container: { flex: 0 },
  input: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 16,
      fontWeight: '400',
      color: '#000',
    }),
    flex: 1,
    height: '100%',
    paddingRight: 2,
  },
  inputContainer: {
    backgroundColor: '#E5E5F0',
    borderBottomWidth: 0,
    borderRadius: 4,
    flex: 0,
    flexDirection: 'row',
    overflow: 'hidden',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 12,
  },
  label: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 16,
      fontWeight: '600',
      color: '#0075bf',
    }),
    marginBottom: 8,
  },
});
