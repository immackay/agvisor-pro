import { observer } from 'mobx-react';
import React from 'react';
import { Image, StyleProp, StyleSheet, View } from 'react-native';
import { Svg, Circle, Text } from 'react-native-svg';

import {
  useAdvisorStore,
  useClientStore,
  useUser,
  OnlineModes,
} from 'stores/User';

const COLORS = [
  '#553d36',
  '#864a52',
  '#8578c5',
  '#87a0b2',
  '#a4bef3',
  '#a2c5ac',
  '#e8dbc5',
  '#ccc4e9',
  '#8ac6d0',
  '#b8f3cc',
];

const DefaultProfilePicture = ({
  color,
  initials,
  size,
  style,
}: {
  color: string;
  initials: string;
  size: number;
  style?: StyleProp<any>;
}) => (
  <View style={style}>
    <Svg height={size} viewBox="0 0 200 200" width={size}>
      <Circle cx={100} cy={100} fill={color} r={100} />
      <Text
        alignmentBaseline={'middle'}
        fill={'#fff'}
        fontSize={90}
        textAnchor={'middle'}
        x={100}
        y={105}>
        {initials}
      </Text>
    </Svg>
  </View>
);

const OnlineIndicator = ({
  online,
  size,
}: {
  online: OnlineModes;
  size: number;
}) => (
  <View
    style={StyleSheet.flatten([
      styles.indicatorStyle,
      {
        backgroundColor:
          online === OnlineModes.OFFLINE
            ? '#999'
            : online === OnlineModes.AWAY
            ? '#F2D024'
            : online === OnlineModes.BUSY
            ? '#cc1f1a'
            : '#5B5',
        width: size,
        height: size,
        borderRadius: size / 2,
      },
    ])}
  />
);

export default observer(
  ({
    userID,
    client,
    size,
    withIndicator,
    withOutline,
  }: {
    userID?: string;
    client?: boolean;
    size: number;
    withIndicator?: boolean;
    withOutline?: boolean;
  }) => {
    const user = userID
      ? client
        ? useClientStore()?.getClient(userID)
        : useAdvisorStore()?.getAdvisor(userID)
      : useUser();

    return (
      <View>
        {user?.id ? (
          user.profile ? (
            <Image
              height={size}
              source={{ uri: user.profile }}
              style={[withOutline && styles.imageStyleOutline]}
              width={size}
            />
          ) : (
            <DefaultProfilePicture
              color={COLORS[Number.parseInt(user.id, 36) % COLORS.length]}
              initials={
                user.firstName!.slice(0, 1) + user.lastName!.slice(0, 1)
              }
              size={size}
              style={[withOutline && styles.imageStyleOutline]}
            />
          )
        ) : (
          <DefaultProfilePicture
            color={COLORS[0]}
            initials={' '}
            size={size}
            style={[withOutline && styles.imageStyleOutline]}
          />
        )}
        {withIndicator && (
          <OnlineIndicator
            online={user?.online ?? OnlineModes.OFFLINE}
            size={size / 4}
          />
        )}
      </View>
    );
  },
);

const styles = StyleSheet.create({
  imageStyleOutline: {
    borderColor: '#fff',
    borderWidth: 2,
  },
  indicatorStyle: {
    borderColor: '#fff',
    borderWidth: 2,
    bottom: 0,
    position: 'absolute',
    right: 0,
  },
});
