import React from 'react';
import {
  Keyboard,
  KeyboardAvoidingView,
  KeyboardAvoidingViewProps,
  Platform,
  TouchableWithoutFeedback,
  View,
  ViewProps,
} from 'react-native';

// TODO investigate need for most of this file
// is this still the best solution? is there something better now?

const DismissableKeyboardHOC = <P extends ViewProps>(
  Comp: React.ElementType,
) => {
  return ({ children, ...props }: React.PropsWithChildren<P>) => (
    <TouchableWithoutFeedback accessible={false} onPress={Keyboard.dismiss}>
      <Comp {...props}>{children}</Comp>
    </TouchableWithoutFeedback>
  );
};

const keyboardAvoidingViewProps =
  Platform.OS === 'ios' ? { behavior: 'padding' } : { behavior: 'padding' };
const PlatformSpecificKeyboardAvoidingView = <
  T extends KeyboardAvoidingViewProps
>(
  props: T,
) => <KeyboardAvoidingView {...keyboardAvoidingViewProps} {...props} />;

const DismissableKeyboardView = DismissableKeyboardHOC(View);
const DismissableKeyboardAvoidingView = DismissableKeyboardHOC<
  KeyboardAvoidingViewProps
>(PlatformSpecificKeyboardAvoidingView);

export {
  DismissableKeyboardHOC,
  DismissableKeyboardView,
  DismissableKeyboardAvoidingView,
};
