import React, {
  Dispatch,
  SetStateAction,
  Fragment,
  PropsWithChildren,
} from 'react';
import {
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  ViewStyle,
} from 'react-native';

import { CheckIcon } from 'assets/svg/icons';
import { getFont } from 'util/Theme';

// passing value & setValue is inconsistent with Input/ErrorMessage and causes unnecessary rerenders
// not necessarily vital to fix though
export const CheckBox = ({
  value,
  setValue,
  label,
  containerStyle,
  style,
  children,
}: PropsWithChildren<{
  value: boolean;
  setValue: Dispatch<SetStateAction<boolean>>;
  label?: string;
  containerStyle?: ViewStyle;
  style?: ViewStyle;
}>) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <View style={[styles.row, style]}>
        <View style={styles.checkbox}>
          <TouchableHighlight
            style={styles.highlight}
            onPress={() => setValue(!value)}>
            {value ? <CheckIcon /> : <Fragment />}
          </TouchableHighlight>
        </View>
        {label ? <Text style={styles.label}>{label}</Text> : <Fragment />}
      </View>
      {children}
    </View>
  );
};

const styles = StyleSheet.create({
  checkbox: { borderColor: '#000', borderWidth: 1, height: 28, width: 28 },
  container: { flex: -1, flexDirection: 'column' },
  highlight: { height: '100%', width: '100%' },
  label: {
    ...getFont({ fontFamily: 'Raleway', fontSize: 16 }),
    flex: 1,
    marginLeft: 12,
  },
  row: { flex: 1, flexDirection: 'row' },
});
