import React, {
  forwardRef,
  useImperativeHandle,
  useMemo,
  useState,
} from 'react';
import { InteractionManager, StyleSheet, Text } from 'react-native';
import Animated, { Easing } from 'react-native-reanimated';
import { useValue } from 'react-native-redash';

import { runTimingWithCallback } from 'util/Animated';
import { getFont } from 'util/Theme';

const { cond, eq, Value } = Animated;

enum ErrorState {
  OFF,
  IN,
  ON,
  OUT,
}

export type ErrorMessageRef = {
  raise(message: string): void;
};
export const ErrorMessage = forwardRef((_, ref) => {
  const [error, setError] = useState('');

  const errorActive = useValue<ErrorState>(ErrorState.OFF);

  const errorClock = useMemo(
    () =>
      cond(
        eq(errorActive, ErrorState.OFF),
        0,
        cond(
          eq(errorActive, ErrorState.IN),
          runTimingWithCallback({
            from: 0,
            to: 1,
            duration: 1000,
            easing: Easing.cubic,
            callback: () => errorActive.setValue(ErrorState.ON),
          }),
          cond(
            eq(errorActive, ErrorState.ON),
            runTimingWithCallback({
              from: 1,
              to: 1,
              duration: 8000,
              easing: Easing.linear,
              callback: () => errorActive.setValue(ErrorState.OUT),
            }),
            cond(
              eq(errorActive, ErrorState.OUT),
              runTimingWithCallback({
                from: 1,
                to: 0,
                duration: 1000,
                easing: Easing.cubic,
                callback: () => errorActive.setValue(ErrorState.OFF),
              }),
            ),
          ),
        ),
      ),
    [errorActive],
  );

  useImperativeHandle(ref, () => ({
    raise: (message: string) =>
      InteractionManager.runAfterInteractions(() => {
        setError(message);
        errorActive.setValue(
          cond(
            eq(errorActive, ErrorState.OFF),
            new Value(ErrorState.IN),
            errorActive,
          ),
        );
      }),
  }));

  return (
    <Animated.View
      style={{
        opacity: errorClock,
      }}>
      <Text style={styles.error}>{error}</Text>
    </Animated.View>
  );
});

const styles = StyleSheet.create({
  error: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 14,
      color: '#f56565',
    }),
    flex: -1,
    lineHeight: 20,
    minHeight: 20,
    textAlignVertical: 'top',
  },
});
