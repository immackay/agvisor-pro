import faker from 'faker';
import _ from 'lodash';

import { Province, Provinces } from 'stores/Auth';
import { OnlineModes } from 'stores/User';

export const provincesAndTerritories = Object.keys(Provinces) as Province[];

export const getRandomProvince = () => _.sample(provincesAndTerritories)!;

export type User = {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  profile: string | null;
  province: Province;
  online: OnlineModes;
};
export type AdvisorExtra = {
  rate: string | null;
  sponsor: string | null;
  description: string | null;
  experience: string | null;
  education: string | null;
  knowledge: string[] | null;
};
export type Advisor = User & AdvisorExtra;
export type Client = User;

const protoUser: User = {
  id: '',
  firstName: '',
  lastName: '',
  email: '',
  province: 'BC',
  profile: null,
  online: OnlineModes.OFFLINE,
};

export const fakeUser = <E>({
  prototype,
  userID,
}: {
  prototype?: User & E;
  userID?: string;
}): User & E => {
  const proto = { ...(prototype ?? protoUser) } as User & E;

  proto.id = userID ?? faker.random.uuid();
  proto.firstName = faker.name.firstName();
  proto.lastName = faker.name.lastName();
  proto.email = faker.internet.email(proto.firstName, proto.lastName);
  proto.province = getRandomProvince();
  proto.online = _.sample(Object.values(OnlineModes))!;

  return proto as any;
};
