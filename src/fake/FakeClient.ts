import { fakeUser, Client } from 'fake/Util';

export const fakeClient = (userID?: string): Client => fakeUser<{}>({ userID });
