import faker from 'faker';

import { Advisor, fakeUser, AdvisorExtra } from 'fake/Util';
import { OnlineModes } from 'stores/User';

const protoAdvisor: Advisor = {
  id: '',
  firstName: '',
  lastName: '',
  email: '',
  province: 'AB',
  profile: null,
  online: OnlineModes.OFFLINE,
  rate: null,
  sponsor: null,
  description: null,
  education: null,
  experience: null,
  knowledge: null,
};

export const fakeAdvisor = (userID?: string): Advisor => {
  const proto = fakeUser<AdvisorExtra>({
    prototype: protoAdvisor,
    userID,
  });

  if (faker.random.boolean()) proto.sponsor = faker.company.companyName();
  else proto.rate = `$${Math.floor(Math.max(Math.random() * 80, 40))} / 10min`;

  proto.description = faker.random.boolean() ? faker.lorem.sentences(3) : null;
  if (faker.random.boolean()) {
    const educationYears = Math.floor(Math.random() * 4) + 1;

    proto.education =
      educationYears +
      ` ${educationYears === 1 ? 'year' : 'years'} | ` +
      faker.lorem.word().replace(/^\w/, (c) => c.toUpperCase());
  }
  proto.experience = faker.random.boolean()
    ? faker.company.companyName()
    : null;
  proto.knowledge = faker.random.boolean()
    ? Array.from({ length: Math.floor(Math.random() * 9 + 1) }, () =>
        faker.lorem.word().replace(/^\w/, (c) => c.toUpperCase()),
      )
    : null;

  return proto;
};
