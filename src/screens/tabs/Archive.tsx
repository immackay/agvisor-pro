import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default () => {
  return (
    <View style={styles.emptyContainer}>
      <Text style={styles.emptyText}>You have no archived sessions.</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  emptyContainer: {
    alignItems: 'center',
    flex: 1,
    padding: 20,
  },
  emptyText: {
    fontSize: 16,
  },
});
