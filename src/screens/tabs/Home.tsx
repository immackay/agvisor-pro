import {
  CompositeNavigationProp,
  useNavigation,
} from '@react-navigation/native';
import { observable } from 'mobx';
import { observer } from 'mobx-react';
import { useLocalStore } from 'mobx-react-lite';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import {
  ChevronIcon,
  ChatIcon,
  ScheduleIcon,
  FavouriteIcon,
} from 'assets/svg/icons';
import { DismissableKeyboardAvoidingView } from 'components/DismissableKeyboardView';
import ProfilePicture from 'components/ProfilePicture';
import {
  SearchBar,
  SearchResultProvider,
  SearchResultsType,
} from 'components/SearchBar';
import {
  TabNavigatorNavigationProp,
  StackNavigatorNavigationProp,
  DrawerNavigatorNavigationProp,
} from 'screens/navigation/Util';
import { AdvisorInstance, useAdvisorStore, OnlineModes } from 'stores/User';
import { getFont } from 'util/Theme';

const EmptyFeatured = () => {
  return (
    <View>
      <Text>There's nothing here!</Text>
    </View>
  );
};

const DetailSection = ({
  title,
  children,
}: {
  title: string;
  children: string | JSX.Element;
}) => (
  <View style={featuredStyles.detailSection}>
    <Text style={featuredStyles.detailTitle}>{title}:</Text>
    <Text style={featuredStyles.detailInfo}>{children}</Text>
  </View>
);

const Featured = observer(
  ({
    userID,
    next,
    prev,
  }: {
    next: () => void;
    prev: () => void;
    userID: string;
  }) => {
    const store = useAdvisorStore()!;
    const advisor = store.getAdvisor(userID);
    const CHAT_COLOR = advisor?.online ? '#0075BF' : '#aaa';
    const SCHEDULE_COLOR = '#76BC32';
    const FAVOURITE_COLOR = '#F4B223';

    const navigation = useNavigation<HomeNavigationProp>();

    if (!advisor) return <View />;

    const {
      firstName,
      lastName,
      description,
      sponsor,
      rate,
      experience,
      education,
      knowledge,
    } = advisor;

    return (
      <View style={featuredStyles.container}>
        <TouchableOpacity style={featuredStyles.prev} onPress={prev}>
          <ChevronIcon color={'#0075bf'} rotate={180} size={32} />
        </TouchableOpacity>
        <View style={featuredStyles.card}>
          <View style={featuredStyles.featured}>
            <View style={featuredStyles.top}>
              <View style={featuredStyles.profile}>
                <ProfilePicture withIndicator size={64} userID={userID} />
              </View>
              <View style={featuredStyles.profileDetails}>
                <Text style={featuredStyles.name}>
                  {firstName} {lastName}
                </Text>
                {sponsor ? (
                  <Text style={featuredStyles.rateLabel}>
                    Sponsored by{' '}
                    <Text style={featuredStyles.rateValue}>{sponsor}</Text>
                  </Text>
                ) : (
                  <Text style={featuredStyles.rateLabel}>
                    Starting at{' '}
                    <Text style={featuredStyles.rateValue}>{rate}</Text>
                  </Text>
                )}
              </View>
            </View>
            {description && description !== '' && (
              <View style={featuredStyles.middle}>
                <Text style={featuredStyles.description}>{description}</Text>
              </View>
            )}
            {(experience || education || knowledge) && (
              <View style={featuredStyles.bottom}>
                {experience && (
                  <>
                    <DetailSection title={'Experience'}>
                      {experience ?? 'error'}
                    </DetailSection>
                  </>
                )}
                {education && (
                  <>
                    <DetailSection title={'Education'}>
                      {education ?? 'error'}
                    </DetailSection>
                  </>
                )}
                {knowledge && (
                  <>
                    <DetailSection title={'Knowledge'}>
                      {knowledge
                        ?.map((k, i) => (i === 0 ? '' : ' | ') + k)
                        .reduce((a, c) => a + c) ?? 'error'}
                    </DetailSection>
                  </>
                )}
              </View>
            )}
          </View>
          <View style={featuredStyles.buttons}>
            <TouchableOpacity
              disabled={
                advisor.online === OnlineModes.OFFLINE ||
                advisor.online === OnlineModes.BUSY
              }
              style={featuredStyles.button}
              onPress={() => navigation.push('New Conversation', { userID })}>
              <ChatIcon
                color={
                  advisor.online === OnlineModes.ONLINE ||
                  advisor.online === OnlineModes.AWAY
                    ? CHAT_COLOR
                    : '#aaa'
                }
              />
              <Text
                style={StyleSheet.flatten([
                  featuredStyles.buttonsText,
                  featuredStyles.buttonChat,
                  {
                    color:
                      advisor.online === OnlineModes.ONLINE ||
                      advisor.online === OnlineModes.AWAY
                        ? CHAT_COLOR
                        : '#aaa',
                  },
                ])}>
                Answer Now
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={featuredStyles.button}>
              <ScheduleIcon />
              <Text
                style={[
                  featuredStyles.buttonsText,
                  featuredStyles.buttonSchedule,
                  { color: SCHEDULE_COLOR },
                ]}>
                Schedule
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={featuredStyles.button}>
              <FavouriteIcon />
              <Text
                style={[
                  featuredStyles.buttonsText,
                  featuredStyles.buttonFavourite,
                  { color: FAVOURITE_COLOR },
                ]}>
                Favourite
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity style={featuredStyles.next} onPress={next}>
          <ChevronIcon color={'#0075bf'} size={32} />
        </TouchableOpacity>
      </View>
    );
  },
);

type HomeNavigationProp = CompositeNavigationProp<
  TabNavigatorNavigationProp<'Home'>,
  CompositeNavigationProp<
    StackNavigatorNavigationProp<'Home'>,
    DrawerNavigatorNavigationProp<'Home'>
  >
>;
export default observer(() => {
  const store = useAdvisorStore()!;
  const advisors = store.getAdvisors();
  const results = useLocalStore(
    (): SearchResultsType<AdvisorInstance> => ({
      data: observable.array(
        advisors?.map((a) => ({ item: a, visible: true })) ?? [],
      ),
    }),
  );
  const [featured, setFeatured] = useState(0);

  const data = useMemo(() => results.data.filter((i) => i.visible), [
    results.data,
  ]);

  useEffect(() => {
    if (featured > data.length) setFeatured(0);
  }, [data.length, featured]);

  // TODO improve, this is relatively broken (probably slow too)
  const searchPredicate = useCallback(
    (i: AdvisorInstance, query) =>
      i.knowledge
        ?.map((k) => !!k.match(new RegExp(query.toLowerCase(), 'gi')))
        .reduce((a, c) => a || c) ?? false,
    [],
  );

  return (
    <DismissableKeyboardAvoidingView style={styles.container}>
      <View style={styles.searchContainer}>
        <SearchResultProvider value={results}>
          <SearchBar
            placeholder={'Get Connected By Category'}
            searchPredicate={searchPredicate}
          />
        </SearchResultProvider>
        <Text style={styles.advisorsOnline}>
          {advisors?.length ?? 0} AGvisors Online
        </Text>
      </View>
      <View style={styles.featured}>
        <Text style={styles.featuredText}>Featured</Text>
        {advisors && data.length > 0 ? (
          <Featured
            next={() => setFeatured((featured + 1) % data.length)}
            prev={() =>
              setFeatured(
                featured -
                  1 -
                  data.length * Math.floor((featured - 1) / data.length),
              )
            }
            userID={data[featured].item.id}
          />
        ) : (
          <EmptyFeatured />
        )}
      </View>
    </DismissableKeyboardAvoidingView>
  );
});
const featuredStyles = StyleSheet.create({
  bottom: { flex: 0, marginTop: 24 },
  button: {
    alignItems: 'center',
    flex: 0,
    justifyContent: 'space-between',
  },
  buttonChat: {},
  buttonFavourite: {},
  buttonSchedule: {},
  buttons: {
    borderTopColor: '#e5e5f0',
    borderTopWidth: 1,
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    minHeight: 70,
    height: 70,
    paddingHorizontal: 24,
    paddingVertical: 12,
    width: '100%',
  },
  buttonsText: { ...getFont({ fontFamily: 'Raleway', textAlign: 'center' }) },
  card: {
    backgroundColor: '#fff',
    borderRadius: 4,
    flex: 1,
    overflow: 'hidden',
  },
  container: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  description: {
    color: '#3d4852',
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 12,
    }),
  },
  detailInfo: {
    color: '#999',
    flex: 2,
    ...getFont({ fontFamily: 'Raleway', fontSize: 12 }),
  },
  detailSection: { flex: 0, flexDirection: 'row', paddingBottom: 8 },
  detailTitle: {
    color: '#999',
    flex: 1,
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 12,
      fontWeight: '600',
    }),
  },
  featured: { flex: 0, padding: 24, paddingBottom: 16 },
  middle: { marginTop: 12 },
  name: {
    color: '#22292f',
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 16,
      fontWeight: '600',
    }),
  },
  next: { flex: 0, marginLeft: 6, marginTop: 128 },
  prev: { flex: 0, marginRight: 6, marginTop: 128 },
  profile: { height: 64, width: 64 },
  profileDetails: { flex: 1, marginLeft: 16 },
  rateLabel: {
    color: '#aaa',
    ...getFont({ fontFamily: 'Raleway', fontSize: 12 }),
  },
  rateValue: {
    color: '#999',
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 14,
      fontWeight: '600',
    }),
  },
  top: { flex: 0, flexDirection: 'row' },
});

const styles = StyleSheet.create({
  advisorsOnline: {
    color: '#999',
    marginTop: 4,
    ...getFont({
      fontSize: 12,
      fontWeight: '700',
      textAlign: 'right',
    }),
  },
  container: {
    height: '100%',
  },
  featured: { flex: 1 },
  featuredText: {
    ...getFont({
      fontSize: 14,
      fontWeight: '700',
    }),
    marginBottom: 24,
    marginLeft: 24,
  },
  searchContainer: {
    paddingHorizontal: 24,
    paddingVertical: 24,
  },
});
