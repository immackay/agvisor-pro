import { observable } from 'mobx';
import { useLocalStore, useObserver } from 'mobx-react-lite';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

import { ChatIcon, FavouriteIcon } from 'assets/svg/icons';
import ProfilePicture from 'components/ProfilePicture';
import {
  SearchBar,
  SearchResultProvider,
  SearchResultsType,
} from 'components/SearchBar';
import { AdvisorInstance, useAdvisorStore, OnlineModes } from 'stores/User';
import { getFont } from 'util/Theme';

const AdvisorListItem = ({ advisor }: { advisor: AdvisorInstance }) => {
  return (
    <View style={itemStyles.container}>
      <View style={itemStyles.top}>
        <View style={itemStyles.profile}>
          <ProfilePicture withIndicator size={48} userID={advisor.id} />
        </View>
        <View style={itemStyles.profileDetails}>
          <View style={itemStyles.topBar}>
            <Text style={itemStyles.name}>
              {advisor.firstName} {advisor.lastName}
            </Text>
            <View style={itemStyles.topButtons}>
              <TouchableOpacity
                disabled={
                  advisor.online === OnlineModes.OFFLINE ||
                  advisor.online === OnlineModes.BUSY
                }
                style={itemStyles.button}>
                <ChatIcon
                  color={
                    advisor.online === OnlineModes.ONLINE ||
                    advisor.online === OnlineModes.AWAY
                      ? '#0075bf'
                      : '#aaa'
                  }
                />
              </TouchableOpacity>
              <TouchableOpacity style={itemStyles.button}>
                <FavouriteIcon />
              </TouchableOpacity>
            </View>
          </View>
          <View style={itemStyles.topDetails}>
            {advisor.sponsor ? (
              <Text style={itemStyles.rateLabel}>
                Sponsored by{' '}
                <Text style={itemStyles.rateValue}>{advisor.sponsor}</Text>
              </Text>
            ) : (
              <Text style={itemStyles.rateLabel}>
                Starting at{' '}
                <Text style={itemStyles.rateValue}>{advisor.rate}</Text>
              </Text>
            )}
          </View>
        </View>
      </View>
      {advisor.description && (
        <View style={itemStyles.middle}>
          <Text style={itemStyles.description}>{advisor.description}</Text>
        </View>
      )}
      {(advisor.experience || advisor.education || advisor.knowledge) && (
        <View style={itemStyles.bottom}>
          {advisor.experience && (
            <Text style={itemStyles.info}>Company: {advisor.experience}</Text>
          )}
          {advisor.education && (
            <Text style={itemStyles.info}>{advisor.education}</Text>
          )}
          {advisor.knowledge && (
            <Text style={itemStyles.info}>
              {advisor.knowledge.map((k, i) => (i === 0 ? '' : ' | ') + k)}
            </Text>
          )}
        </View>
      )}
    </View>
  );
};

export default () => {
  const advisors = useAdvisorStore()?.getAdvisors();

  const results = useLocalStore(
    (): SearchResultsType<AdvisorInstance> => ({
      data: observable.array(
        advisors?.map((a) => ({ item: a, visible: true })) ?? [],
      ),
    }),
  );

  return useObserver(() => (
    <View style={styles.container}>
      {advisors && (
        <SearchResultProvider value={results}>
          <FlatList
            data={results.data.filter((i) => i.visible)}
            keyboardDismissMode={'on-drag'}
            keyExtractor={({ item }) => item.id}
            ListHeaderComponent={
              <View style={styles.search}>
                <SearchBar<AdvisorInstance>
                  placeholder={'Search...'}
                  searchPredicate={(advisor, query) =>
                    advisor.name.includes(query)
                  }
                />
              </View>
            }
            renderItem={({
              item,
            }: {
              item: { visible: boolean; item: AdvisorInstance };
            }) => <AdvisorListItem advisor={item.item} />}
            style={styles.list}
          />
        </SearchResultProvider>
      )}
    </View>
  ));
};

const itemStyles = StyleSheet.create({
  bottom: { flex: 2, marginTop: 12 },
  button: {
    alignItems: 'center',
    borderRadius: 15,
    flex: 0,
    height: 30,
    justifyContent: 'center',
    overflow: 'hidden',
    width: 30,
  },
  container: {
    borderBottomColor: '#e5e5f0',
    borderBottomWidth: 1,
    paddingHorizontal: 24,
    paddingVertical: 12,
  },
  description: {
    color: '#3d4852',
    ...getFont({ fontSize: 12, fontFamily: 'Raleway' }),
  },
  info: { color: '#999', ...getFont({ fontSize: 12, fontFamily: 'Raleway' }) },
  middle: { flex: 1, marginTop: 12 },
  name: {
    ...getFont({ fontSize: 14, fontWeight: '600', fontFamily: 'Raleway' }),
  },
  profile: { height: 48, width: 48 },
  profileDetails: { flex: 1, marginLeft: 12, width: '100%' },
  rateLabel: {
    color: '#aaa',
    ...getFont({ fontSize: 12, fontFamily: 'Raleway' }),
  },
  rateValue: {
    color: '#999',
    ...getFont({ fontSize: 14, fontWeight: '600', fontFamily: 'Raleway' }),
  },
  top: { flex: 2, flexDirection: 'row' },
  topBar: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  topButtons: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  topDetails: {
    flex: 1,
  },
});

const styles = StyleSheet.create({
  container: { flex: 1 },
  list: {},
  search: {
    padding: 24,
  },
});
