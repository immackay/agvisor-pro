import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { StyleSheet } from 'react-native';

import {
  AGvisorPROIcon,
  AGvisorsIcon,
  ArchiveIcon,
  ForumIcon,
} from 'assets/svg/icons';
import { AGvisors, Archive, Forum, Home } from 'screens/tabs';

const Tab = createBottomTabNavigator();

export default () => (
  <Tab.Navigator
    tabBarOptions={{
      activeTintColor: '#0075bf',
      inactiveTintColor: '#BDBDBD',
      tabStyle: styles.tab,
      style: styles.tabBar,
    }}>
    <Tab.Screen
      component={Home}
      name={'Home'}
      options={{ tabBarIcon: AGvisorPROIcon }}
    />
    <Tab.Screen
      component={Archive}
      name={'Session Archive'}
      options={{ tabBarIcon: ArchiveIcon, title: 'Archive' }}
    />
    <Tab.Screen
      component={Forum}
      name={'Forum'}
      options={{ tabBarIcon: ForumIcon }}
    />
    <Tab.Screen
      component={AGvisors}
      name={'AGvisors'}
      options={{ tabBarIcon: AGvisorsIcon }}
    />
  </Tab.Navigator>
);

const styles = StyleSheet.create({
  tab: {
    paddingTop: 10,
  },
  tabBar: {
    backgroundColor: '#FFF',
  },
});
