import { BottomTabNavigationProp } from '@react-navigation/bottom-tabs';
import { DrawerNavigationProp } from '@react-navigation/drawer';
import { RouteProp, useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper';

import { HomeIcon } from 'assets/svg/icons';
import ProfilePicture from 'components/ProfilePicture';

export const getHeaderTitle = (route: any) =>
  route.state
    ? route.state.routes[route.state.index].name
    : route.params?.screen ?? 'Home';

export const MenuButton = () => {
  const navigation = useNavigation<DrawerNavigatorNavigationProp<'Home'>>();

  return (
    <TouchableOpacity style={styles.menuButton} onPress={navigation.openDrawer}>
      <ProfilePicture size={36} />
    </TouchableOpacity>
  );
};

export const HomeButton = ({ tintColor }: { tintColor?: string }) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity onPress={() => navigation.navigate('Home')}>
      <HomeIcon color={tintColor} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  fakeMenuButton: {
    backgroundColor: '#f4b223',
    borderRadius: 18,
    height: 36,
    width: 36,
  },
  menuButton: {
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 20,
    height: 40,
    justifyContent: 'center',
    marginBottom: isIphoneX() ? 8 : 0,
    width: 40,
  },
});

export type RootTabParamList = {
  Home: undefined;
  'Session Archive': undefined;
  Forum: undefined;
  AGvisors: undefined;
};
export type RootStackParamList = {
  Home: undefined;
  'AGvisor Profile': undefined;
  'New Session Request': undefined;
  Tag: undefined;
  'New Conversation': {
    userID: string;
  };
};
export type RootDrawerParamList = {
  Home: undefined;
  'My Profile': undefined;
  Favourites: undefined;
  Schedules: undefined;
  FAQ: undefined;
  Tour: undefined;
};

export type TabNavigatorRouteProp<T extends keyof RootTabParamList> = RouteProp<
  RootTabParamList,
  T
>;
export type StackNavigatorRouteProp<
  T extends keyof RootStackParamList
> = RouteProp<RootStackParamList, T>;
export type DrawerNavigatorRouteProp<
  T extends keyof RootDrawerParamList
> = RouteProp<RootDrawerParamList, T>;

export type TabNavigatorNavigationProp<
  T extends keyof RootTabParamList
> = BottomTabNavigationProp<RootTabParamList, T>;
export type StackNavigatorNavigationProp<
  T extends keyof RootStackParamList
> = StackNavigationProp<RootStackParamList, T>;
export type DrawerNavigatorNavigationProp<
  T extends keyof RootDrawerParamList
> = DrawerNavigationProp<RootDrawerParamList, T>;
