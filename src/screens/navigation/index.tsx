import AsyncStorage from '@react-native-community/async-storage';
import {
  NavigationContainer,
  NavigationContainerRef,
} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { observer } from 'mobx-react';
import React, {
  forwardRef,
  Ref,
  useCallback,
  useEffect,
  useState,
  Fragment,
} from 'react';
import { Platform, Linking } from 'react-native';

import Loading from 'screens/Loading';
import { AuthNavigator } from 'screens/auth';
import { useAuth } from 'stores/Auth';
import { Logger } from 'util/Logger';
import { getModulePath } from 'util/getModulePath';

import Drawer from './Drawer';

const logger = new Logger(getModulePath(__filename));

const BaseStack = createStackNavigator();

export const Navigation = observer(
  forwardRef((_, ref: Ref<NavigationContainerRef>) => {
    const auth = useAuth();
    const [isReady, setIsReady] = useState(!__DEV__);
    const [initialState, setInitialState] = useState(undefined);

    const onStateChange = useCallback(
      (state) =>
        AsyncStorage.setItem('navigationPersistenceKey', JSON.stringify(state)),
      [],
    );

    useEffect(() => {
      if (__DEV__ && !isReady) {
        const restoreState = async () => {
          logger.info('Restoring navigation state...', { path: 'Navigation' });
          try {
            const initialUrl = await Linking.getInitialURL();

            // Only restore state if there's no deep link and we're not on web
            if (Platform.OS !== 'web' && initialUrl == null) {
              const savedStateString = await AsyncStorage.getItem(
                'navigationPersistenceKey',
              );
              const state = savedStateString
                ? JSON.parse(savedStateString)
                : undefined;

              if (state !== undefined) setInitialState(state);
            }
          } finally {
            setIsReady(true);
          }
        };

        restoreState()
          .then(() =>
            logger.info('Successfully restored navigation state', {
              path: 'Navigation',
            }),
          )
          .catch((e) =>
            logger.warn('Error restoring navigation state', {
              path: 'navigation',
              item: e,
            }),
          );
      }
    }, [isReady]);

    if (!isReady) return <Loading />;

    return (
      <NavigationContainer
        ref={ref}
        initialState={initialState}
        onStateChange={__DEV__ ? onStateChange : undefined}>
        {/* TODO change animation for switching to/from auth */}
        <BaseStack.Navigator headerMode={'none'}>
          <BaseStack.Screen component={AuthNavigator} name={'Auth'} />
          {auth.isAuthorized ? (
            <BaseStack.Screen component={Drawer} name={'Home'} />
          ) : (
            <Fragment />
          )}
        </BaseStack.Navigator>
      </NavigationContainer>
    );
  }),
);
