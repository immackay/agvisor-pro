import {
  createStackNavigator,
  HeaderBackButton,
  StackHeaderLeftButtonProps,
} from '@react-navigation/stack';
import React, { ComponentType } from 'react';

import { BackArrowIcon } from 'assets/svg/icons';
import { NewConversation, Profile, Schedule, Tag } from 'screens/stack';

import Tab from './Tab';
import { HomeButton, MenuButton, getHeaderTitle } from './Util';

export const Stack = createStackNavigator();
export const StackOptions: Omit<
  React.ComponentProps<typeof Stack.Navigator>,
  'children'
> = {
  screenOptions: {
    headerStyle: {
      backgroundColor: '#0375BF',
    },
    headerTitleStyle: {
      color: '#FFF',
    },
    headerLeftContainerStyle: {
      marginLeft: 12,
    },
    headerLeft: ({ canGoBack, ...props }: StackHeaderLeftButtonProps) =>
      canGoBack ? (
        <HeaderBackButton
          {...props}
          backImage={({ tintColor }) => <BackArrowIcon color={tintColor} />}
          labelVisible={false}
          tintColor={'#fff'}
        />
      ) : (
        <MenuButton />
      ),
  },
};
export const StackOptionsWithHome: Omit<
  React.ComponentProps<typeof Stack.Navigator>,
  'children'
> = {
  ...StackOptions,
  screenOptions: {
    ...StackOptions.screenOptions,
    headerRight: (props) => <HomeButton {...props} />,
    headerRightContainerStyle: {
      marginRight: 12,
    },
  },
};

export const wrapWithStack = (
  screen: ComponentType<any>,
  name: string,
  withHome?: boolean,
): ComponentType<any> => () => (
  <Stack.Navigator {...(withHome ? StackOptionsWithHome : StackOptions)}>
    <Stack.Screen component={screen} name={name} />
  </Stack.Navigator>
);

export default () => (
  <Stack.Navigator {...StackOptions}>
    <Stack.Screen
      component={Tab}
      name={'Home'}
      options={({ route }) => ({
        headerTitle: getHeaderTitle(route),
        headerLeft: () => <MenuButton />,
      })}
    />
    <Stack.Screen component={Profile} name={'AGvisor Profile'} />
    <Stack.Screen component={Schedule} name={'New Session Request'} />
    <Stack.Screen component={Tag} name={'Tag'} options={{ title: 'Tag' }} />
    <Stack.Screen component={NewConversation} name={'New Conversation'} />
  </Stack.Navigator>
);
