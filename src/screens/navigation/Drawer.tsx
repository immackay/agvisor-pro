import {
  createDrawerNavigator,
  DrawerContentComponentProps,
  DrawerContentOptions,
  DrawerContentScrollView,
  DrawerItem,
} from '@react-navigation/drawer';
import {
  DrawerDescriptorMap,
  DrawerNavigationHelpers,
} from '@react-navigation/drawer/lib/typescript/src/types';
import {
  CommonActions,
  DrawerActions,
  DrawerNavigationState,
  NavigationState,
  PartialState,
  Route,
  useLinkBuilder,
  StackActions,
} from '@react-navigation/native';
import { observer } from 'mobx-react';
import React, { useCallback, useMemo, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { ChevronIcon, CrossIcon } from 'assets/svg/icons';
import ProfilePicture from 'components/ProfilePicture';
import { translate, Translations } from 'i18n';
import {
  FAQ,
  Favourites,
  Feedback,
  MyProfile,
  Schedules,
  Tour,
} from 'screens/drawer';
import becomeAgvisorPlaceholder from 'screens/drawer/agvisor';
import addFarmPlaceholder from 'screens/drawer/farm';
import { useAuth } from 'stores/Auth';
import { useUser } from 'stores/User';
// import { Logger } from 'util/Logger';
import { Storage } from 'util/Storage';
import { getFont } from 'util/Theme';
// import { getModulePath } from 'util/getModulePath';

import Stack, { wrapWithStack } from './Stack';

// TODO cleanup
// TODO go over implementation

// const logger = new Logger(getModulePath(__filename));

const Drawer = createDrawerNavigator();

const DrawerDivider = ({ color }: { color?: string }) => (
  <View style={[styles.divider, { backgroundColor: color ?? '#000' }]} />
);

const Home = observer(
  ({
    navigation,
    route: { key, name },
  }: {
    navigation: DrawerNavigationHelpers;
    route: Route<string> & {
      state?: NavigationState | PartialState<NavigationState>;
    };
    i: number;
  }) => {
    const user = useUser();

    return (
      <TouchableOpacity
        key={key}
        style={styles.homeContainer}
        onPress={() => {
          navigation.dispatch(CommonActions.navigate(name));
        }}>
        <View>
          <ProfilePicture size={48} />
        </View>
        <View style={styles.homeMiddle}>
          <Text style={styles.homeName}>{user.name}</Text>
          <Text style={styles.homeEmail}>{user.email ?? 'error'}</Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.dispatch(DrawerActions.closeDrawer())}>
          <View>
            <CrossIcon />
          </View>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  },
);

const CustomDrawerItemList = ({
  state,
  navigation,
  descriptors,
  activeTintColor,
  inactiveTintColor,
  activeBackgroundColor,
  inactiveBackgroundColor,
  itemStyle,
  labelStyle,
  ...rest
}: Omit<DrawerContentOptions, 'contentContainerStyle' | 'style'> & {
  state: DrawerNavigationState;
  navigation: DrawerNavigationHelpers;
  descriptors: DrawerDescriptorMap;
}) => {
  const auth = useAuth();
  const buildLink = useLinkBuilder();
  const [supportMenuVisible, setSupportMenuVisible] = useState(false);

  const {
    newRoutes,
    home,
    faq,
    tour,
    feedback,
    addFarm,
    becomeAdvisor,
  } = useMemo(() => {
    const { routes } = state;

    enum separateRoutes {
      'Home',
      'FAQ',
      'Tour',
      'Feedback',
      'Add A Farm',
      'Become An AGvisor',
    }

    return {
      newRoutes: routes.filter(({ name }) => !(name in separateRoutes)),
      home: routes.find(({ name }) => name === separateRoutes[0]),
      faq: routes.find(({ name }) => name === separateRoutes[1]),
      tour: routes.find(({ name }) => name === separateRoutes[2]),
      feedback: routes.find(({ name }) => name === separateRoutes[3]),
      addFarm: routes.find(({ name }) => name === separateRoutes[4]),
      becomeAdvisor: routes.find(({ name }) => name === separateRoutes[5]),
    };
  }, [state]);

  const createDrawerItem = useCallback(
    (
      route: Route<string> & {
        state?: NavigationState | PartialState<NavigationState>;
      },
      i: number,
      activeBackgroundColorOverride?: string,
    ) => {
      const focused = i === state.index;
      const { title, drawerLabel, drawerIcon } = descriptors[route.key].options;

      return (
        <DrawerItem
          key={route.key}
          activeBackgroundColor={
            activeBackgroundColorOverride ?? activeBackgroundColor
          }
          activeTintColor={activeTintColor}
          focused={focused}
          icon={drawerIcon}
          inactiveBackgroundColor={inactiveBackgroundColor}
          inactiveTintColor={inactiveTintColor}
          label={
            drawerLabel !== undefined
              ? drawerLabel
              : title !== undefined
              ? title
              : route.name
          }
          labelStyle={labelStyle}
          style={itemStyle}
          to={buildLink(route.name, route.params)}
          onPress={() => {
            navigation.dispatch({
              ...(focused
                ? DrawerActions.closeDrawer()
                : CommonActions.navigate(route.name)),
              target: state.key,
            });
          }}
        />
      );
    },
    [
      activeBackgroundColor,
      activeTintColor,
      buildLink,
      descriptors,
      inactiveBackgroundColor,
      inactiveTintColor,
      itemStyle,
      labelStyle,
      navigation,
      state.index,
      state.key,
    ],
  );

  let routesCount = 0;

  return (
    <>
      {home ? (
        <Home i={routesCount++} navigation={navigation} route={home} />
      ) : (
        <></>
      )}
      <DrawerDivider color={activeTintColor} />
      {newRoutes.map((route) => createDrawerItem(route, routesCount++))}
      {(!!faq || !!tour || !!feedback) && (
        <>
          <DrawerItem
            {...{
              state,
              navigation,
              descriptors,
              activeTintColor,
              inactiveTintColor,
              activeBackgroundColor,
              inactiveBackgroundColor,
              itemStyle,
              labelStyle,
              ...rest,
            }}
            key={-1}
            label={({ color }) => (
              <View
                style={StyleSheet.flatten([
                  itemStyle,
                  { flexDirection: 'row' },
                ])}>
                <Text
                  style={StyleSheet.flatten([
                    labelStyle,
                    { color, marginRight: 12 },
                  ])}>
                  {translate(Translations.navigation.drawer.support)}
                </Text>
                <ChevronIcon
                  color={'#fff'}
                  rotate={supportMenuVisible ? 270 : 90}
                  size={16}
                />
              </View>
            )}
            onPress={() => setSupportMenuVisible(!supportMenuVisible)}
          />
          {supportMenuVisible && (
            <View style={styles.supportMenu}>
              {faq && createDrawerItem(faq, routesCount++, '#519DCD')}
              {tour && createDrawerItem(tour, routesCount++, '#519DCD')}
              {feedback && createDrawerItem(feedback, routesCount++, '#519DCD')}
            </View>
          )}
        </>
      )}
      <DrawerItem
        {...{
          state,
          navigation,
          descriptors,
          activeTintColor,
          inactiveTintColor,
          activeBackgroundColor,
          inactiveBackgroundColor,
          itemStyle,
          labelStyle,
          ...rest,
        }}
        key={-2}
        label={translate(Translations.navigation.drawer.signOut)}
        onPress={async () => {
          navigation.dispatch(StackActions.replace('Auth'));
          await auth.signOut();
          Storage.clear();
        }}
      />
      <DrawerDivider color={activeTintColor} />
      {addFarm && createDrawerItem(addFarm, routesCount++)}
      {becomeAdvisor && createDrawerItem(becomeAdvisor, routesCount++)}
    </>
  );
};

const CustomDrawerContent = (props: DrawerContentComponentProps) => (
  <DrawerContentScrollView {...props}>
    <CustomDrawerItemList {...props} />
  </DrawerContentScrollView>
);

const CustomDrawerContentOptions: React.ComponentProps<
  typeof Drawer.Navigator
>['drawerContentOptions'] = {
  activeTintColor: '#fff',
  activeBackgroundColor: '#0386DD',
  inactiveTintColor: '#fff',
  inactiveBackgroundColor: undefined,
  itemStyle: {
    maxHeight: 48,
  },
  labelStyle: {
    ...getFont({ fontFamily: 'Raleway', fontSize: 16 }),
  },
  contentContainerStyle: {},
  style: {},
};

export default () => (
  <Drawer.Navigator
    drawerContent={CustomDrawerContent}
    drawerContentOptions={CustomDrawerContentOptions}
    drawerStyle={styles.drawer}
    drawerType={'front'}>
    <Drawer.Screen component={Stack} name={'Home'} />
    <Drawer.Screen
      component={wrapWithStack(MyProfile, 'My Profile', true)}
      name={'My Profile'}
      options={{
        drawerLabel: translate(Translations.navigation.drawer.myProfile),
      }}
    />
    <Drawer.Screen
      component={wrapWithStack(Favourites, 'Favourites', true)}
      name={'Favourites'}
      options={{
        drawerLabel: translate(Translations.navigation.drawer.favourites),
      }}
    />
    <Drawer.Screen
      component={wrapWithStack(Schedules, 'Schedules', true)}
      name={'Schedules'}
      options={{
        drawerLabel: translate(Translations.navigation.drawer.schedules),
      }}
    />
    <Drawer.Screen
      component={wrapWithStack(FAQ, 'FAQ', true)}
      name={'FAQ'}
      options={{
        drawerLabel: translate(Translations.navigation.drawer.faq),
      }}
    />
    <Drawer.Screen
      component={wrapWithStack(Tour, 'Tour', true)}
      name={'Tour'}
      options={{
        drawerLabel: translate(Translations.navigation.drawer.tour),
      }}
    />
    <Drawer.Screen
      component={wrapWithStack(Feedback, 'Feedback', true)}
      name={'Feedback'}
      options={{
        drawerLabel: translate(Translations.navigation.drawer.feedback),
      }}
    />
    <Drawer.Screen
      component={wrapWithStack(addFarmPlaceholder, 'Add A Farm', true)}
      name={'Add A Farm'}
      options={{
        drawerLabel: `+ ${translate(Translations.navigation.drawer.addFarm)}`,
      }}
    />
    <Drawer.Screen
      component={wrapWithStack(
        becomeAgvisorPlaceholder,
        'Become An AGvisor',
        true,
      )}
      name={'Become An AGvisor'}
      options={{
        drawerLabel: `+ ${translate(
          Translations.navigation.drawer.becomeAgvisor,
        )}`,
      }}
    />
  </Drawer.Navigator>
);

const styles = StyleSheet.create({
  divider: {
    height: 1,
    marginVertical: 12,
    opacity: 0.5,
    width: '100%',
  },
  drawer: {
    backgroundColor: '#0375BF',
    width: '100%',
  },
  homeContainer: {
    flex: 0,
    flexDirection: 'row',
    padding: 24,
    justifyContent: 'space-between',
  },
  homeEmail: {
    color: '#fff',
    ...getFont({ fontFamily: 'Raleway', fontSize: 16 }),
  },
  homeMiddle: {
    flex: 1,
    paddingLeft: 16,
  },
  homeName: {
    color: '#fff',
    ...getFont({ fontFamily: 'Raleway', fontSize: 24, fontWeight: '600' }),
  },
  supportMenu: {
    backgroundColor: '#388bbf',
    paddingLeft: 40,
  },
});
