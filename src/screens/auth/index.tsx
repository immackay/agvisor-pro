import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';

import Agreements from './Agreements';
import ForgotPassword from './ForgotPassword';
import Login from './Login';
import PersonalInformation from './PersonalInformation';
import Verification from './Verification';
import Welcome from './Welcome';

const AuthStack = createStackNavigator();

export const AuthNavigator = () => {
  return (
    <AuthStack.Navigator
      screenOptions={{
        // TODO remove header from AuthContainer and replace with react-navigation header
        headerShown: false,
      }}>
      <AuthStack.Screen component={Login} name={'Login'} />
      <AuthStack.Screen
        component={PersonalInformation}
        name={'Personal Information'}
      />
      <AuthStack.Screen component={Agreements} name={'Agreements'} />
      <AuthStack.Screen component={Verification} name={'Verification'} />
      <AuthStack.Screen component={ForgotPassword} name={'Forgot Password'} />
      <AuthStack.Screen component={Welcome} name={'Welcome'} />
    </AuthStack.Navigator>
  );
};
