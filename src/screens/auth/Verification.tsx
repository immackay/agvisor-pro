import { useNavigation } from '@react-navigation/native';
import React, { useCallback } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper';

import { AGvisorPROIconLarge } from 'assets/svg/icons';
import AuthContainer from 'screens/auth/AuthContainer';
// import { Logger } from 'util/Logger';
import { TestIDs } from 'util/TestID';
import { getFont } from 'util/Theme';
import { getModulePath } from 'util/getModulePath';

// TODO fill out information text
// TODO ensure proper handling of all errors

// const logger = new Logger(getModulePath(__filename));
export const testIDs = TestIDs(getModulePath(__filename));

export default () => {
  const navigation = useNavigation();
  const goToLogin = useCallback(() => navigation.navigate('Login'), [
    navigation,
  ]);

  return (
    <AuthContainer goBack={goToLogin} testID={testIDs.base}>
      <View style={styles.icon}>
        <AGvisorPROIconLarge size={175} />
      </View>
      <TouchableOpacity style={styles.loginButton} onPress={goToLogin}>
        <Text style={styles.loginButtonText}>Back to Login</Text>
      </TouchableOpacity>
    </AuthContainer>
  );
};

const styles = StyleSheet.create({
  form: {},
  formItem: {
    paddingBottom: 24,
  },
  icon: {
    alignItems: 'center',
    paddingBottom: 32,
    paddingTop: isIphoneX() ? 84 : 40,
  },
  loginButton: {
    alignItems: 'center',
    backgroundColor: '#0075bf',
    borderRadius: 4,
    height: 50,
    justifyContent: 'center',
  },
  loginButtonText: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 16,
      color: '#fff',
      fontWeight: '600',
    }),
  },
});
