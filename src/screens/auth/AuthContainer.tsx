import { useBackHandler } from '@react-native-community/hooks';
import React, { PropsWithChildren, ReactNode, Fragment, useRef } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewProps,
  ScrollView,
  KeyboardAvoidingView,
  ViewStyle,
  InteractionManager,
  StatusBar,
} from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper';

import LoginCircle from 'assets/svg/LoginCircle';
import { BackArrowIcon } from 'assets/svg/icons';
import { getFont } from 'util/Theme';

type ScrollTo = ({
  x,
  y,
  animated,
}: {
  x?: number;
  y?: number;
  animated?: boolean;
}) => void;
export const AuthContainerContext = React.createContext<undefined | ScrollTo>(
  undefined,
);

export default ({
  children,
  style,
  containerStyle,
  goBack,
  title,
  iconRight,
  ...rest
}: PropsWithChildren<
  ViewProps & {
    containerStyle?: ViewStyle;
    goBack?: () => void;
    title?: string;
    iconRight?: () => ReactNode;
  }
>) => {
  const useHeader = !!goBack || !!title || !!iconRight;
  const scrollViewRef = useRef<ScrollView>(null);
  useBackHandler(() => {
    if (goBack) {
      goBack();
      return true;
    }

    return false;
  });

  return (
    <View {...rest} style={styles.container}>
      <StatusBar animated barStyle={'dark-content'} />
      {useHeader ? (
        <View style={styles.header}>
          <StatusBar animated barStyle={'light-content'} />
          <View style={styles.headerSection}>
            {goBack && (
              <TouchableOpacity
                hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                style={styles.back}
                onPress={() => InteractionManager.runAfterInteractions(goBack)}>
                <BackArrowIcon color={'#fff'} />
              </TouchableOpacity>
            )}
          </View>
          <View style={[styles.headerSection, styles.headerTitle]}>
            {title && <Text style={styles.title}>{title}</Text>}
          </View>
          <View style={styles.headerSection}>{iconRight}</View>
        </View>
      ) : (
        <Fragment />
      )}
      <ScrollView
        ref={scrollViewRef}
        contentContainerStyle={containerStyle}
        style={[styles.children, style]}>
        <View style={styles.childrenContainer}>
          <KeyboardAvoidingView>
            <AuthContainerContext.Provider
              value={scrollViewRef.current?.scrollTo}>
              {children}
            </AuthContainerContext.Provider>
          </KeyboardAvoidingView>
        </View>
      </ScrollView>
      <View style={styles.circle}>
        <LoginCircle />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  back: {},
  children: {
    flex: 1,
  },
  childrenContainer: { marginHorizontal: 22 },
  circle: {
    bottom: -120,
    opacity: 0.1,
    position: 'absolute',
    right: -60,
    zIndex: -100,
  },
  container: {
    flex: 1,
  },
  header: {
    alignItems: 'center',
    backgroundColor: '#0075bf',
    flexDirection: 'row',
    flex: -1,
    height: isIphoneX() ? 96 : 72,
    justifyContent: 'center',
  },
  headerSection: {
    alignItems: 'center',
    flex: 1,
    height: '100%',
    justifyContent: isIphoneX() ? 'flex-end' : 'center',
    marginBottom: isIphoneX() ? 48 : 0,
  },
  headerTitle: { flex: 3 },
  title: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 16,
      fontWeight: '600',
      color: '#fff',
    }),
  },
});
