import { useNavigation, StackActions } from '@react-navigation/native';
import React, { useCallback, useRef, useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper';
import validate from 'validate.js';

import { AGvisorPROIconLarge } from 'assets/svg/icons';
import { EmailInput, InputRef, PasswordInput } from 'components/Input';
import Loading from 'screens/Loading';
import AuthContainer from 'screens/auth/AuthContainer';
import { useAuth } from 'stores/Auth';
import { user } from 'stores/User';
import { Logger } from 'util/Logger';
import { TestID, TestIDs } from 'util/TestID';
import { getFont } from 'util/Theme';
import { Constraints } from 'util/Validation';
import { getModulePath } from 'util/getModulePath';

// TODO ensure proper handling of all errors
// TODO change TouchableOpacity import to 'react-native-gesture-handler' after
//   https://github.com/software-mansion/react-native-gesture-handler/pull/1104 is in release
//   (replace style with containerStyle)

const logger = new Logger(getModulePath(__filename));
export const testIDs = TestIDs(
  getModulePath(__filename),
  TestID('emailInput', ['Input', 'email']),
  TestID('passwordInput', ['Input', 'password']),
  TestID('loginButton', ['Button', 'login']),
  TestID('forgotPasswordButton', ['Button', 'forgotPassword']),
  TestID('createAccountButton', ['Button', 'createAccount']),
);

export default () => {
  const auth = useAuth();
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);

  const emailRef = useRef<InputRef>(null);
  const passwordRef = useRef<InputRef>(null);

  const login = useCallback(() => {
    if (emailRef.current && passwordRef.current) {
      const { value: email } = emailRef.current;
      const { value: password } = passwordRef.current;

      const data = { email, password };

      const validation = validate(data, Constraints.login);

      if (validation) {
        if (validation.email) emailRef.current.shake(validation.email[0]);
        if (validation.password)
          passwordRef.current.shake(validation.password[0]);

        logger.info('Login failed validation', {
          name: 'login',
          item: { validation, data: { email, password: '**hidden**' } },
        });
      } else {
        logger.info('Login passed validation, attempting login', {
          name: 'login',
        });

        setLoading(true);
        auth
          .loginWithEmailAndPassword(email, password)
          .then(async (result) => {
            const { success, message, redirect } = result;

            if (success) {
              logger.info('Login success, switching to app', { name: 'login' });
              await user.login();
              navigation.dispatch(StackActions.replace('Home'));
              setLoading(false);
            } else {
              setLoading(false);
              if (redirect && redirect === 'verifyContact') {
                logger.info('Login failure, switching to verification', {
                  name: 'login',
                });

                navigation.navigate('Verification');
              } else {
                if (result.errorCode) {
                  switch (result.errorCode) {
                    case 'NotAuthorizedException':
                      passwordRef.current?.shake(message);
                      return;
                    case 'UserNotFoundException':
                      emailRef.current?.shake(message);
                      return;
                  }
                }

                logger.warn(
                  `Login failure: ${
                    result.error
                      ? `${result.errorCode ?? result.error.code} ${
                          result.error
                        }`
                      : 'an error occurred'
                  }`,
                  { name: 'login', item: result.error },
                );
              }
            }
          })
          .catch((e) =>
            logger.warn('Login failure: an error occurred', {
              name: 'login',
              item: e,
            }),
          );
      }
    }
  }, [auth, navigation]);

  if (loading) return <Loading />;

  return (
    <AuthContainer testID={testIDs.base}>
      <View style={styles.icon}>
        <AGvisorPROIconLarge size={175} />
      </View>
      <EmailInput
        ref={emailRef}
        containerStyle={styles.formItem}
        testID={testIDs.emailInput}
      />
      <PasswordInput
        ref={passwordRef}
        containerStyle={styles.formItem}
        testID={testIDs.passwordInput}
      />
      <TouchableOpacity
        style={styles.loginButton}
        testID={testIDs.loginButton}
        onPress={login}>
        <Text style={styles.loginButtonText}>Login Now !</Text>
      </TouchableOpacity>
      <View style={styles.extra}>
        <TouchableOpacity
          hitSlop={{ top: 30, bottom: 30, left: 30, right: 0 }}
          style={styles.extraButton}
          testID={testIDs.forgotPasswordButton}
          onPress={() => navigation.navigate('Forgot Password')}>
          <Text
            style={StyleSheet.flatten([
              styles.extraText,
              { textAlign: 'right' },
            ])}>
            Forgot Password?
          </Text>
        </TouchableOpacity>
        <Text style={[styles.extraText, styles.extraDivider]}>|</Text>
        <TouchableOpacity
          hitSlop={{ top: 30, bottom: 30, left: 0, right: 30 }}
          style={styles.extraButton}
          testID={testIDs.createAccountButton}
          onPress={() => navigation.navigate('Personal Information')}>
          <Text
            style={StyleSheet.flatten([
              styles.extraText,
              { textAlign: 'left' },
            ])}>
            Create Account
          </Text>
        </TouchableOpacity>
      </View>
    </AuthContainer>
  );
};

const styles = StyleSheet.create({
  extra: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 48,
  },
  extraButton: { flex: 1 },
  extraDivider: {
    marginHorizontal: 4,
  },
  extraText: {
    flex: -1,
    ...getFont({ fontFamily: 'Raleway', fontSize: 14 }),
  },
  form: {},
  formItem: {
    paddingBottom: 24,
  },
  icon: {
    alignItems: 'center',
    paddingBottom: 32,
    paddingTop: isIphoneX() ? 84 : 40,
  },
  loginButton: {
    alignItems: 'center',
    backgroundColor: '#0075bf',
    borderRadius: 4,
    height: 50,
    justifyContent: 'center',
  },
  loginButtonText: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 16,
      color: '#fff',
      fontWeight: '600',
    }),
  },
});
