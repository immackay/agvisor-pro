import { useLayout } from '@react-native-community/hooks';
import { Picker } from '@react-native-community/picker';
import { useNavigation } from '@react-navigation/native';
import React, {
  memo,
  PropsWithChildren,
  ReactText,
  useCallback,
  useContext,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  Modal,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { PanGestureHandler } from 'react-native-gesture-handler';
import validate from 'validate.js';

import { ChevronIcon } from 'assets/svg/icons';
import { CheckBox } from 'components/CheckBox';
import { ErrorMessage, ErrorMessageRef } from 'components/ErrorMessage';
import { EmailInput, Input, InputRef, PasswordInput } from 'components/Input';
import { ShakeView, ShakeViewRef } from 'components/ShakeView';
import Loading from 'screens/Loading';
import AuthContainer from 'screens/auth/AuthContainer';
import { AuthResult, Provinces, useAuth } from 'stores/Auth';
import { Logger } from 'util/Logger';
import { TestID, TestIDs } from 'util/TestID';
import { getFont } from 'util/Theme';
import { Constraints } from 'util/Validation';
import { getModulePath } from 'util/getModulePath';

const logger = new Logger(getModulePath(__filename));
export const testIDs = TestIDs(
  getModulePath(__filename),
  TestID('firstNameInput', ['Input', 'firstName']),
  TestID('lastNameInput', ['Input', 'lastName']),
  TestID('emailInput', ['Input', 'email']),
  TestID('passwordInput', ['Input', 'password']),
  TestID('confirmPasswordInput', ['Input', 'confirmPassword']),
);

const PickerWrapper = memo(({ children }: PropsWithChildren<{}>) => {
  const [visible, setVisible] = useState(false);
  const province = useContext(ProvinceContext);
  const open = useCallback(() => setVisible(true), []);
  const close = useCallback(() => setVisible(false), []);
  const { onLayout: onLayoutChildren, ...childrenLayout } = useLayout();
  const { onLayout: onLayoutModal, ...modalLayout } = useLayout();

  const panGestureHandler = useCallback(
    ({ nativeEvent: { velocityY } }: { nativeEvent: { velocityY: number } }) =>
      velocityY >= -10 && close(),
    [close],
  );

  // TODO better ios solution
  return (
    <>
      <TouchableOpacity style={styles.province} onPress={open}>
        <Text style={styles.provinceText}>
          {province ? Provinces[province] : 'Province'}
        </Text>
        <ChevronIcon color={'#0075bf'} rotate={90} size={15} />
      </TouchableOpacity>
      <Modal transparent animationType={'slide'} visible={visible}>
        <View style={styles.modal} onLayout={onLayoutModal}>
          <View style={styles.modalChildren} onLayout={onLayoutChildren}>
            <View style={styles.closeBar} />
            {children}
          </View>
          <View style={styles.closeModal}>
            <TouchableOpacity
              style={[
                styles.buttonCloseModal,
                {
                  height: modalLayout.height - childrenLayout.height * 1.5,
                },
              ]}
              onPress={close}
            />
            <PanGestureHandler onGestureEvent={panGestureHandler}>
              <View
                style={[
                  styles.panCloseModal,
                  {
                    height: childrenLayout.height,
                  },
                ]}
              />
            </PanGestureHandler>
          </View>
        </View>
      </Modal>
    </>
  );
});

const PickProvince = memo(
  ({ onChange }: { onChange: (value: ReactText) => void }) => {
    const province = useContext(ProvinceContext);
    const entries = useMemo(
      () =>
        Object.entries(Provinces).map(([key, value], i) => (
          <Picker.Item key={i} label={value} value={key} />
        )),
      [],
    );

    return (
      <Picker
        itemStyle={styles.pickerItem}
        mode={'dropdown'}
        selectedValue={province}
        style={[
          styles.picker,
          Platform.OS === 'ios' && styles.pickerIOS,
          Platform.OS === 'android' && styles.pickerAndroid,
        ]}
        onValueChange={onChange}>
        {entries}
      </Picker>
    );
  },
);

const ProvinceContext = React.createContext<keyof typeof Provinces | undefined>(
  undefined,
);

export default () => {
  const auth = useAuth();
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);

  const firstNameRef = useRef<InputRef>(null);
  const lastNameRef = useRef<InputRef>(null);
  const emailRef = useRef<InputRef>(null);
  const passwordRef = useRef<InputRef>(null);
  const confirmPasswordRef = useRef<InputRef>(null);
  const provinceErrorRef = useRef<ErrorMessageRef>(null);
  const provinceShakeRef = useRef<ShakeViewRef>(null);
  const termsErrorRef = useRef<ErrorMessageRef>(null);
  const termsShakeRef = useRef<ShakeViewRef>(null);
  const [province, setProvince] = useState<keyof typeof Provinces | undefined>(
    Platform.OS === 'android' ? 'AB' : undefined,
  );

  const [agreedToTerms, setAgreedToTerms] = useState(false);

  // TODO doesn't do anything
  const [agreedToOffers, setAgreedToOffers] = useState(false);

  const signUp = useCallback(() => {
    if (
      firstNameRef.current &&
      lastNameRef.current &&
      emailRef.current &&
      passwordRef.current &&
      confirmPasswordRef.current
    ) {
      const { value: firstName } = firstNameRef.current;
      const { value: lastName } = lastNameRef.current;
      const { value: email } = emailRef.current;
      const { value: password } = passwordRef.current;
      const { value: confirmPassword } = confirmPasswordRef.current;

      const data = {
        firstName,
        lastName,
        email,
        password,
        confirmPassword,
        province,
        agreedToTerms,
      };

      const validation = validate(data, Constraints.signUp);

      if (validation) {
        if (validation.email) emailRef.current.shake(validation.email[0]);
        if (validation.password)
          passwordRef.current.shake(validation.password[0]);
        if (validation.confirmPassword)
          confirmPasswordRef.current.shake(validation.confirmPassword[0]);
        if (validation.firstName)
          firstNameRef.current.shake(validation.firstName[0]);
        if (validation.lastName)
          lastNameRef.current.shake(validation.lastName[0]);
        if (validation.province) {
          provinceShakeRef.current?.shake();
          provinceErrorRef.current?.raise(validation.province[0]);
        }
        if (validation.agreedToTerms) {
          termsShakeRef.current?.shake();
          termsErrorRef.current?.raise(validation.agreedToTerms[0]);
        }

        logger.info('Signup failed validation', {
          name: 'signUp',
          item: {
            validation,
            data: {
              ...data,
              password: '**hidden**',
              confirmPassword: '**hidden**',
            },
          },
        });
      } else {
        logger.info('Signup passed validation, attempting signup', {
          name: 'signUp',
        });

        setLoading(true);
        auth
          .signUpWithEmailAndPassword(email, password, {
            firstName,
            lastName,
            province: province!,
          })
          .then((result: AuthResult) => {
            const { success, message } = result;

            if (success) {
              logger.info('Signup success, switching to verification', {
                name: 'signUp',
              });

              navigation.navigate('Verification');
            } else {
              setLoading(false);
              if (result.errorCode) {
                switch (result.errorCode) {
                  case 'invalid_password':
                    passwordRef.current?.shake(message);
                    return;
                }
              }

              logger.warn(
                `Signup failure: ${
                  result.error
                    ? `${result.errorCode ?? result.error.code ?? ''}`
                    : 'an error occurred'
                }`,
                { name: 'signUp', item: result.error },
              );
            }
          })
          .catch((e) =>
            logger.warn('Signup failure: an error occured', {
              name: 'signUp',
              item: e,
            }),
          );
      }
    }
  }, [agreedToTerms, auth, navigation, province]);

  const updateProvince = useCallback(
    (value) => setProvince(value as keyof typeof Provinces),
    [],
  );

  const RenderPicker = useCallback(
    () =>
      Platform.OS === 'ios' ? (
        <PickerWrapper>
          <PickProvince onChange={updateProvince} />
        </PickerWrapper>
      ) : (
        <PickProvince onChange={updateProvince} />
      ),
    [updateProvince],
  );

  if (loading) return <Loading />;

  return (
    <AuthContainer
      containerStyle={styles.container}
      goBack={() => navigation.navigate('Login')}
      testID={testIDs.base}
      title={'Personal Information'}>
      <Text style={[styles.formItem, styles.formText]}>
        Please tell us your name and provide an email address we can use for
        your account. We will send you an email with a verification link to
        confirm your email address.
      </Text>
      <Text style={[styles.formItem, styles.formText]}>
        Once you have verified your email address, you will be ready to use
        AGvisorPRO.
      </Text>
      <Input
        ref={firstNameRef}
        containerStyle={styles.formItem}
        label={'First Name'}
        placeholder={'First Name'}
      />
      <Input
        ref={lastNameRef}
        containerStyle={styles.formItem}
        label={'Last Name'}
        placeholder={'Last Name'}
      />
      <EmailInput ref={emailRef} containerStyle={styles.formItem} />
      <View style={styles.pickerContainer}>
        <Text style={styles.pickerLabel}>Province</Text>
        <ShakeView ref={provinceShakeRef} style={styles.pickerWrapper}>
          <ProvinceContext.Provider value={province}>
            <RenderPicker />
          </ProvinceContext.Provider>
        </ShakeView>
        <ErrorMessage ref={provinceErrorRef} />
      </View>
      <PasswordInput ref={passwordRef} containerStyle={styles.formItem} />
      <PasswordInput
        ref={confirmPasswordRef}
        containerStyle={styles.formItem}
        label={'Confirm Password'}
        placeholder={'Confirm Password'}
      />
      <ShakeView ref={termsShakeRef}>
        <CheckBox
          containerStyle={styles.checkBoxContainer}
          label={'Agree to Terms & Conditions'}
          setValue={setAgreedToTerms}
          value={agreedToTerms}>
          <ErrorMessage ref={termsErrorRef} />
        </CheckBox>
      </ShakeView>
      <CheckBox
        containerStyle={styles.formItem}
        label={
          "I would like to receive offers from AGvisorPRO's 3rd party partner organizations"
        }
        setValue={setAgreedToOffers}
        value={agreedToOffers}
      />
      <TouchableOpacity style={styles.signUp} onPress={signUp}>
        <Text style={styles.signUpText}>Sign Up</Text>
      </TouchableOpacity>
    </AuthContainer>
  );
};

const styles = StyleSheet.create({
  buttonCloseModal: {
    position: 'relative',
    width: '100%',
  },
  checkBoxContainer: { paddingBottom: 4 },
  closeBar: {
    backgroundColor: '#888',
    borderRadius: 5,
    height: 5,
    width: 50,
  },
  closeModal: {
    height: '100%',
    position: 'absolute',
    width: '100%',
  },
  container: {
    paddingVertical: 32,
  },
  formItem: {
    paddingBottom: 24,
  },
  formText: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 14,
    }),
  },
  modal: {
    alignItems: 'center',
    height: '100%',
    justifyContent: 'flex-end',
    width: '100%',
  },
  modalChildren: {
    alignItems: 'center',
    backgroundColor: '#F0F0F0',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    flex: -1,
    justifyContent: 'center',
    paddingHorizontal: 12,
    paddingTop: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: -1 },
    shadowRadius: 3,
    width: '100%',
    zIndex: 1,
  },
  panCloseModal: {
    position: 'relative',
    width: '100%',
  },
  picker: { width: '100%' },
  pickerAndroid: { height: 50, marginLeft: 8 },
  pickerContainer: { paddingBottom: 4 },
  pickerIOS: {
    backgroundColor: '#F0F0F0',
    borderRadius: 4,
  },
  pickerItem: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 16,
    }),
  },
  pickerLabel: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 16,
      fontWeight: '600',
      color: '#0075bf',
    }),
    marginBottom: 8,
  },
  pickerWrapper: {
    alignItems: 'center',
    backgroundColor: '#E5E5F0',
    borderRadius: 4,
    height: 50,
    justifyContent: 'center',
    width: '100%',
  },
  province: {
    alignItems: 'center',
    backgroundColor: '#E5E5F0',
    borderRadius: 4,
    flexDirection: 'row',
    height: 50,
    justifyContent: 'space-between',
    paddingHorizontal: 12,
    width: '100%',
  },
  provinceText: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 16,
      color: '#000',
    }),
  },
  signUp: {
    alignItems: 'center',
    backgroundColor: '#0075bf',
    borderRadius: 4,
    height: 50,
    justifyContent: 'center',
  },
  signUpText: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 16,
      color: '#fff',
      fontWeight: '600',
    }),
  },
});
