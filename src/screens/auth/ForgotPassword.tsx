import { useNavigation } from '@react-navigation/native';
import React, { useCallback, useRef } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper';

import { AGvisorPROIconLarge } from 'assets/svg/icons';
import { Input, InputRef } from 'components/Input';
import AuthContainer from 'screens/auth/AuthContainer';
import { Logger } from 'util/Logger';
import { TestIDs } from 'util/TestID';
import { getFont } from 'util/Theme';
import { getModulePath } from 'util/getModulePath';

const logger = new Logger(getModulePath(__filename));
export const testIDs = TestIDs(getModulePath(__filename));

export default () => {
  const navigation = useNavigation();
  const emailRef = useRef<InputRef>(null);

  const sendEmail = useCallback(() => {
    logger.warn('UNIMPLEMENTED', { name: 'sendEmail' });
  }, []);

  return (
    <AuthContainer
      goBack={() => navigation.navigate('Login')}
      testID={testIDs.base}
      title={'Forgot Password'}>
      <View style={styles.icon}>
        <AGvisorPROIconLarge size={175} />
      </View>
      <Input
        ref={emailRef}
        autoCapitalize={'none'}
        autoCompleteType={'email'}
        clearButtonMode={'while-editing'}
        containerStyle={styles.formItem}
        keyboardType={'email-address'}
        label={'Email Address'}
        placeholder={'Email Address'}
        textContentType={'emailAddress'}
      />
      <TouchableOpacity style={styles.sendEmail} onPress={sendEmail}>
        <Text style={styles.sendEmailText}>Send Email</Text>
      </TouchableOpacity>
    </AuthContainer>
  );
};

const styles = StyleSheet.create({
  form: {},
  formItem: {
    paddingBottom: 24,
  },
  icon: {
    alignItems: 'center',
    paddingBottom: 32,
    paddingTop: isIphoneX() ? 84 : 40,
  },
  sendEmail: {
    alignItems: 'center',
    backgroundColor: '#0075bf',
    borderRadius: 4,
    height: 50,
    justifyContent: 'center',
  },
  sendEmailText: {
    ...getFont({
      fontFamily: 'Raleway',
      fontSize: 16,
      color: '#fff',
      fontWeight: '600',
    }),
  },
});
