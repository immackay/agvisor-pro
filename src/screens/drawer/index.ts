import FAQ from './FAQ';
import Favourites from './Favourites';
import Feedback from './Feedback';
import MyProfile from './MyProfile';
import Schedules from './Schedules';
import Tour from './Tour';

export { FAQ, Favourites, Feedback, MyProfile, Schedules, Tour };
