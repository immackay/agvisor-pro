module.exports = {
  testRunner: 'jest',
  runnerConfig: 'jest.config.js',
  configurations: {
    ios: {
      type: 'ios.simulator',
      binaryPath:
        'ios/build/Build/Products/Staging-iphonesimulator/[Staging] AGvisorPRO.app',
      device: {
        type: 'iPhone 11 Pro',
      },
    },
    android: {
      type: 'android.emulator',
      binaryPath:
        'android/app/build/outputs/apk/releaseStaging/app-releaseStaging.apk',
      device: {
        avdName: 'API_29',
      },
    },
  },
};
