module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
  },
  env: {
    es2020: true,
    node: true,
    jest: true,
    browser: true,
  },
  plugins: [
    '@typescript-eslint',
    'prettier',
    'jsdoc',
    'promise',
    'eslint-comments',
    'jest',
  ],
  extends: [
    '@react-native-community',
    'plugin:import/recommended',
    'plugin:import/typescript',
    'plugin:jsdoc/recommended',
    'plugin:promise/recommended',
    'plugin:eslint-comments/recommended',
    'plugin:jest/recommended',
    'plugin:jest/style',
  ],
  rules: {
    'react/jsx-sort-props': [
      'error',
      {
        callbacksLast: true,
        ignoreCase: true,
        reservedFirst: true,
        shorthandFirst: true,
      },
    ],
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'error',
    'react-native/no-inline-styles': 'warn',
    'react-native/sort-styles': 'error',
    'import/order': [
      'error',
      {
        alphabetize: {
          order: 'asc',
        },
        groups: [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
        ],
        'newlines-between': 'always',
      },
    ],
    curly: ['error', 'multi-or-nest', 'consistent'],
    '@typescript-eslint/no-non-null-assertion': 'off',
    '@typescript-eslint/prefer-nullish-coalescing': 'error',
    '@typescript-eslint/prefer-optional-chain': 'error',
    'import/no-named-as-default': 'off',
    'promise/always-return': 'off',
    'promise/catch-or-return': 'off',
  },
  settings: {
    'import/ignore': ['node_modules'],
    'import/resolver': {
      'babel-module': {},
    },
  },
};
