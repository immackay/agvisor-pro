const env = process.env.JEST_ENV;

const shared = {
  preset: 'react-native',
  testEnvironment: 'jsdom',
  testRunner: 'jest-circus/runner',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|otf|ttf|webp|svg)$': '<rootDir>/__mocks__/file.js',
  },
  setupFilesAfterEnv: ['<rootDir>/__tests__/shared.setup.js'],
  setupFiles: [
    '<rootDir>/node_modules/react-native-gesture-handler/jestSetup.js',
  ],
};

const detoxConfig = {
  ...shared,
  displayName: 'e2e',
  testEnvironment: '<rootDir>/__tests__/detoxEnvironment.setup.js',
  testTimeout: 120000,
  testRegex: '(/__tests__/.*(\\.|/)(e2e))\\.([jt]sx?)?$',
  reporters: ['detox/runners/jest/streamlineReporter'],
  verbose: true,
};

const jestConfig = {
  ...shared,
  displayName: 'unit',
  testRegex: '(/__tests__/.*(\\.|/)(test|spec))\\.([jt]sx?)?$',
  setupFilesAfterEnv: [
    ...shared.setupFilesAfterEnv,
    '<rootDir>/__tests__/jest.setup.js',
  ],
  snapshotSerializers: ['enzyme-to-json/serializer'],
};

const lintConfig = {
  ...shared,
  displayName: 'lint',
  runner: 'jest-runner-eslint',
  testRegex: '.*\\.([jt]sx?)$',
  modulePathIgnorePatterns: ['<rootDir>/(android|ios)/'],
};

const projects = [];

if (env === 'all') projects.push(lintConfig, jestConfig, detoxConfig);
else if (env === 'e2e') projects.push(detoxConfig);
else if (env === 'lint') projects.push(lintConfig);
else projects.push(jestConfig);

module.exports = {
  projects,
};
