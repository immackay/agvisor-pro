const hide = jest.fn();
const show = jest.fn();

export { hide, show };
export default { hide, show };
